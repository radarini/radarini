#!/bin/sh

set -e

echo "Creating dump"

mkdir -p /app/data/dumps
python manage.py dumpdata \
    --natural-foreign \
    --natural-primary \
    --exclude contenttypes \
    --exclude auth.Permission \
    --exclude admin.logentry \
    --indent 2 \
    --output /app/data/dumps/dump-$(date +%FT%H%M).json;

echo "Cleaning up old dumps"

cd /app/data/dumps
files=$(ls -t | tail -n +44)
if [[ -n "$files" ]]; then
    xargs rm <<< "$files"
fi

echo "Updating symlink to latest dump"

newest_file=$(ls -t | head -n 1)
ln -fs $newest_file latest.json
