#!/bin/sh

set -e

HOST="$1"
ERROR_REPORT_MAIL="$2"

if [[ "$HOST" = "localhost" || "$HOST" = "" ]]; then
    echo "Detected localhost"
    if [ -f "/etc/letsencrypt/live/localhost/privkey.pem" ]; then
        echo "SSL exists - exiting certbot container"
        exit 0
    fi
    echo "No SSL - Creating self-signed certificate"
    mkdir -p /etc/letsencrypt/live/localhost
    openssl req -x509 \
    -out /etc/letsencrypt/live/localhost/fullchain.pem \
    -keyout /etc/letsencrypt/live/localhost/privkey.pem \
    -newkey rsa:2048 \
    -nodes -sha256 \
    -subj '/CN=localhost' \
    -extensions EXT \
    -config <(printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth");
    echo "SSL created - exiting certbot container"
    exit 0
fi

ipv4_port_regex='^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(:[0-9]{1,5})?$'
if [[ $HOST =~ $ipv4_port_regex ]]; then
    echo "Detected IP address as host (SSL certificates can not be created for IPs)  - exiting certbot container."
    exit 1
fi

trap exit TERM
while true; do
    certbot certonly \
        --webroot \
        --webroot-path /var/www/certbot/ \
        -m "$ERROR_REPORT_MAIL" \
        --agree-tos -n \
        -d "$HOST"
    sleep 12h & wait $!
done
