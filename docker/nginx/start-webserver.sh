#!/bin/sh

set -e

if [[ "$DOMAIN" = "" ]]; then
    DOMAIN="localhost"
fi

if [ ! -f "/etc/letsencrypt/live/$DOMAIN/privkey.pem" ]; then

    if [ ! "$DOMAIN" = "localhost" ]; then
        echo "Detected production host without ssl"
        echo "Starting server for certbot challenge"
        envsubst '$$DOMAIN' < /etc/nginx/conf.d/letsencrypt.conf.template > /etc/nginx/nginx.conf
        nginx -g 'daemon on;'
        while [ ! -f "/etc/letsencrypt/live/$DOMAIN/privkey.pem" ]; do
            echo "Waiting for certbot challenge to finish"
            sleep 1
        done
        echo "SSL created - continuing"
        nginx -s stop
        while pidof nginx >/dev/null; do
            echo "Stopping server for certbot challenge"
            sleep 1
        done
    fi

    if [ "$DOMAIN" = "localhost" ]; then
        echo "Detected localhost without ssl"
        while [ ! -f "/etc/letsencrypt/live/$DOMAIN/privkey.pem" ]; do
            echo "Waiting for ssl to be created"
            sleep 1
        done
        echo "SSL created - continuing"
    fi

fi

if [ "${HTTP_AUTH_PASSWORD}" != "" ]; then
  sed -i "s/#auth_basic/auth_basic/g;" /etc/nginx/conf.d/default.conf.template
  rm -rf /etc/nginx/.htpasswd
  echo -n $HTTP_AUTH_LOGIN:$(openssl passwd -apr1 $HTTP_AUTH_PASSWORD) >> /etc/nginx/.htpasswd
  echo "Basic auth is on for user ${HTTP_AUTH_LOGIN}..."
else
  echo "Basic auth is off (HTTP_AUTH_PASSWORD not provided)"
fi


echo "Starting production server"
envsubst '$$DOMAIN $$INDEX $$RADARINI_PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/nginx.conf &&
nginx -g 'daemon off;'
