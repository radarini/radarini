from django.views import View
from django.shortcuts import render
from django.views.generic.edit import DeleteView
from django.core.exceptions import PermissionDenied
from parler.views import TranslatableCreateView, TranslatableUpdateView
from django.contrib.gis.geos import Point
from apps.locations.forms import PhysicalLocationForm
from apps.locations.models import PhysicalLocation
from apps.locations.searches import searchAddressOSM, searchFeaturesDB, searchPoiOSM
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import SimpleLazyObject
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator

import json


class PhysicalLocationCreateView(TranslatableCreateView):
    """
    Create a physicallocation form with initial data.

    The initial data will be loaded from the `data` parameter of the request.
    If there are osm_tags provided, those will be extracted to physicallocation attributes.
    """

    model = PhysicalLocation
    success_url = "/locations/success?type=create"

    def get_form_class(self):  # noqa: D102
        return PhysicalLocationForm

    def get_initial(self):  # noqa: D102
        initial = super().get_initial()
        get_data = self.request.GET.dict().get("data")

        if isinstance(get_data, list):
            raise Exception("Multiple data values given. A single value is needed.")
        if get_data:
            initial.update(json.loads(get_data))

        lat = initial.get("latitude")
        lon = initial.get("longitude")
        if lon and lat:
            initial["coordinates"] = Point(float(lon), float(lat))

        osm_data = {}
        osm_tags = initial.get("osm_tags")
        if osm_tags:
            osm_data = PhysicalLocation.osm_extractor(osm_tags)

        initial.update({**osm_data})
        return initial


class PhysicalLocationUpdateView(TranslatableUpdateView):
    """Update an existing physicallocation form with."""

    success_url = "/locations/success?type=update"

    # Doing it explicit here, because when just providing the model
    # variable it will always use the fallback and not the current language
    def get_object(self, queryset=None):  # noqa: D102
        pk = self.kwargs.get("pk")
        object = PhysicalLocation.objects.get(pk=pk)
        return object

    def get_context_data(self, **kwargs):  # noqa: D102
        context_data = super().get_context_data(**kwargs)
        context_data["request_path"] = self.request.path
        return context_data

    def get_form_class(self):  # noqa: D102
        return PhysicalLocationForm

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not request.user.is_staff:
            raise PermissionDenied
        return super(PhysicalLocationUpdateView, self).dispatch(
            request, *args, **kwargs
        )


class PhysicalLocationDeleteView(DeleteView):
    """Delete physicallocation object."""

    success_url = "/locations/success?type=delete"
    model = PhysicalLocation

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not request.user.is_staff:
            raise PermissionDenied
        return super(PhysicalLocationDeleteView, self).dispatch(
            request, *args, **kwargs
        )


class PhysicalLocationSuccessView(View):
    """Confirm message for location action."""

    def get(self, request):  # noqa: D102
        type = request.GET.get("type")
        return render(request, "locations/confirm.html", {"type": type})


@method_decorator(never_cache, name="dispatch")
class LocationSearchResults(View):
    """Search for address in OSM and DB and display results in a list."""

    def get(self, request):  # noqa: D102
        site = SimpleLazyObject(lambda: get_current_site(request))
        region = site.settings.region
        countrycode = site.settings.country_code
        search = request.GET.dict().get("search")
        excludes = PhysicalLocation.objects.values_list("osm_id", flat=True).distinct()
        resultsAddressOSM = searchAddressOSM(search, region, countrycode)
        resultsPoiOSM = searchPoiOSM(search, region, countrycode, excludes)
        resultsDB = searchFeaturesDB(search)
        return render(
            request,
            "locations/search_result.html",
            {
                "resultsAddressOSM": resultsAddressOSM,
                "resultsPoiOSM": resultsPoiOSM,
                "resultsDB": resultsDB,
            },
        )
