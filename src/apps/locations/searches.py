"""Physicallocation search.

This module proovides functionality to search for addresses and locations in the db and on osm.
"""

import json
import requests
import os
from apps.locations.models import PhysicalLocation
from django.db.models import Q

HEADERS = {"User-Agent": os.environ.get("INSTANCE_NAME", "radarini")}


def searchAddressOSM(search: str, region: str, countrycode: str) -> list:
    """
    Search addresses for the given string in the osm.

    Results are not linked to single objects in the osm.
    """
    if not search:
        return []
    nominatimSearchUrl = (
        f"https://nominatim.openstreetmap.org/search?"
        f"q={search},{region}&"
        f"format=geojson&"
        f"countrycodes={countrycode}&"
        f"layer=address&"
        f"limit=4&"
        f"addressdetails=1&"
        f"extratags=1"
    )
    response = requests.get(nominatimSearchUrl, headers=HEADERS)
    if response.status_code != 200:
        raise Exception(f"Network response was not ok: {response.status_code}")
    geoJsonOSM = json.loads(response.text)
    featuresOSM = []
    for feature in geoJsonOSM["features"]:
        feature["properties"]["osm_id"] = ""
        feature["properties"]["osm_type"] = ""
        try:
            featuresOSM.append(geoJson2result(feature))
        except KeyError:
            continue
    return featuresOSM


def searchPoiOSM(search: str, region: str, countrycode: str, exclude: list) -> list:
    """
    Search POIs for the given string in the osm.

    Results are linked to single objects in the osm.
    """
    if not search:
        return []
    nominatimSearchUrl = (
        f"https://nominatim.openstreetmap.org/search?"
        f"q={search},{region}&"
        f"format=geojson&"
        f"countrycodes={countrycode}&"
        f"layer=poi&"
        f"limit=4&"
        f"addressdetails=1&"
        f"extratags=1"
    )
    response = requests.get(nominatimSearchUrl, headers=HEADERS)
    if response.status_code != 200:
        raise Exception(f"Network response was not ok: {response.status_code}")
    geoJsonOSM = json.loads(response.text)
    featuresOSM = []
    for feature in geoJsonOSM["features"]:
        if feature["properties"]["osm_id"] in exclude:
            continue
        try:
            featuresOSM.append(geoJson2result(feature))
        except KeyError:
            continue
    return featuresOSM


def geoJson2result(feature: dict) -> dict:
    """Transform a given geojson feature into an objet of the search result list."""
    prop = feature["properties"]

    address_data = {
        "osm_id": prop["osm_id"],
        "osm_type": prop["osm_type"],
        "name": prop.get("name", ""),
        "postal_code": prop["address"]["postcode"],
        "address_region": prop["address"]["city"],
        "street_address": f"{prop['address']['road']} {prop['address'].get('house_number', '')}",
        "latitude": feature["geometry"]["coordinates"][1],
        "longitude": feature["geometry"]["coordinates"][0],
        "osm_tags": prop.get("extratags", {}),
    }

    address_data["display_name"] = (
        f"{address_data['name']} {address_data['street_address']}, "
        f"{address_data['postal_code']} {address_data['address_region']}"
    )

    return address_data


def searchFeaturesDB(search: str) -> list:
    """Search physicallocations for the given string in the database."""
    featuresDB = []
    if not search:
        return []
    queryset = PhysicalLocation.objects.filter(
        Q(street_address__icontains=search)
        | Q(name__icontains=search)
        | Q(postal_code__icontains=search)
        | Q(address_region__icontains=search)
        | Q(address_region__icontains=search)
    ).distinct()
    for feature in queryset:
        featuresDB.append({"display_name": feature, "db_id": feature.pk})

    return featuresDB
