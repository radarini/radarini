from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import AreaOfRelevance, PhysicalLocation
from parler.admin import TranslatableAdmin

admin.site.register(AreaOfRelevance, MPTTModelAdmin)
admin.site.register(PhysicalLocation, admin_class=TranslatableAdmin)
