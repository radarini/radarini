from django.apps import AppConfig


class LocationsConfig(AppConfig):  # noqa: D101
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.locations"
