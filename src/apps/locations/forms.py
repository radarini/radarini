from apps.locations.models import PhysicalLocation
from django.forms.widgets import TextInput, Textarea
from parler.forms import TranslatableModelForm
from django import forms
from django.contrib.gis.geos import Point


class PointFieldWidget(forms.MultiWidget):
    """Widget for the PointField, that creates two seperate input fields for the coordinates of a Point object."""

    def __init__(self, attrs=None):
        widgets = (
            forms.NumberInput(attrs={"x-ref": "lng"}),
            forms.NumberInput(attrs={"x-ref": "lat"}),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        """Return the two values of the Point object, if available."""
        if isinstance(value, Point):
            return [value.x, value.y]
        return [None, None]

    def value_from_datadict(self, data, files, name):
        """Create a Point object from the input data in a dict."""
        x = data.get(name + "_0")
        y = data.get(name + "_1")
        if x and y:
            try:
                return Point(float(x), float(y))
            except ValueError:
                pass
        return None


class PhysicalLocationForm(TranslatableModelForm):
    """
    Form to create a PhysicalLocation object.

    The fields for the geo data are read only, because those should not be
    changed by the user. This form should be prepopulated with the correct
    goe data. Name and accessibility fields can be changed.
    """

    class Meta:
        model = PhysicalLocation
        fields = [
            "name",
            "street_address",
            "postal_code",
            "address_region",
            "wheelchair_accessible",
            "wheelchair_accessible_toilet",
            "wheelchair_accessible_description",
            "blind_description",
            "tactile_writing",
            "tactile_paving",
            "osm_id",
            "osm_type",
            "coordinates",
        ]
        widgets = {
            "street_address": TextInput(attrs={"readonly": "true"}),
            "postal_code": TextInput(attrs={"readonly": "true"}),
            "address_region": TextInput(attrs={"readonly": "true"}),
            "osm_id": TextInput(attrs={"readonly": "true"}),
            "osm_type": TextInput(attrs={"readonly": "true"}),
            "coordinates": PointFieldWidget(attrs={"readonly": "true"}),
            "wheelchair_accessible_description": Textarea(
                attrs={
                    "class": "w-full border-[#d1d5db]",
                    "x-data": "{resize: () => {$el.style.height = '5px'; $el.style.height = $el.scrollHeight + 'px'}}",
                    "x-init": "resize()",
                    "@input": "resize()",
                }
            ),
            "blind_description": Textarea(
                attrs={
                    "class": "w-full border-[#d1d5db]",
                    "x-data": "{resize: () => {$el.style.height = '5px'; $el.style.height = $el.scrollHeight + 'px'}}",
                    "x-init": "resize()",
                    "@input": "resize()",
                }
            ),
        }
