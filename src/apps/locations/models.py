from django.db import models
from django.utils import timezone
from django.utils.translation import get_language, gettext_lazy as _
from django.contrib.gis.db.models import PointField

from parler.models import TranslatableModel, TranslatedFields, TranslatedField
from mptt.models import MPTTModel, TreeForeignKey

import re


class BaseModel(models.Model):
    """BaseModel class to set some common fields like created_at and updated_at."""

    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:  # noqa: D106
        abstract = True


class PhysicalLocation(TranslatableModel, BaseModel):
    """A Location is a physical address.

    Attributes:
        name                                A unique name for the location, e.g. the name of the organization.
                                            Relates to osm tag 'name'.
        street_address                      Street name and house number combined.
                                            Relates to osm tag 'addr:street' and 'addr:housenumber'.
        postal_code                         Digits of the postal code.
                                            Relates to osm tag 'addr:postcode'.
        address_region                      Name of the region.
                                            Relates to osm tag 'addr:city'.
        wheelchair_accessible               Is the location accessible with a wheelchair?
                                            Relates to osm tag 'wheelchair'.
        wheelchair_accessible_toilet        Are there toilets, that are accessible with a wheelchair?
                                            Relates to osm tag 'toilets:wheelchair'.
        wheelchair_accessible_description   Instructions how the location can be accessed by wheelchair.
                                            Relates to osm tags 'wheelchair:description*'.
        blind_description                   Instructions how the location can be accessed by blind people.
                                            Relates to osm tags 'blind:description*'.
        tactile_writing                     Is there text that can be read with the fingers?
                                            Relates to osm tags 'tactile_writing*'
        tactile_paving                      Is there a textured ground surface to guide you?
                                            Relates to osm tag 'tactile_paving'.
        coordinates                         GPS coordinates of the location.
        osm_id                              Id of the object in the osm, that is related to this location.
        osm_type                            Type of the object in the osm, that is related to this location.
    """

    name = models.CharField(
        _("name"),
        help_text=_(
            "A unique name for the location, e.g. the name of the organization."
        ),
        blank=True,
        max_length=150,
    )
    street_address = models.CharField(
        _("street address"),
        help_text=_("Street name and house number combined."),
        max_length=150,
    )
    postal_code = models.CharField(
        _("postal code"),
        help_text=_("Digits of the postal code."),
        max_length=20,
    )
    address_region = models.CharField(
        _("address region"),
        help_text=_("Name of the region."),
        max_length=30,
    )
    ACCESSIBILITY_CHOICES = [
        ("no", _("No")),
        ("yes", _("Yes")),
        ("limited", _("Limited")),
    ]
    wheelchair_accessible = models.CharField(
        _("wheelchair accessible"),
        max_length=10,
        help_text=_("Is the location accessible with a wheelchair?"),
        choices=ACCESSIBILITY_CHOICES,
        default="no",
    )
    wheelchair_accessible_toilet = models.BooleanField(
        _("wheelchair accessible toilet"),
        help_text=_("Are there toilets, that are accessible with a wheelchair?"),
        default=False,
    )
    wheelchair_accessible_description = TranslatedField(any_language=True)
    blind_description = TranslatedField(any_language=True)
    translations = TranslatedFields(
        wheelchair_accessible_description=models.TextField(
            _("description of wheelchair accessibility"),
            help_text=_("Instructions how the location can be accessed by wheelchair."),
            blank=True,
        ),
        blind_description=models.TextField(
            _("description of accessibility for blind people"),
            help_text=_(
                "Instructions how the location can be accessed by blind people. "
            ),
            blank=True,
        ),
    )
    tactile_writing = models.BooleanField(
        _("Tactile writing"),
        help_text=_("Is there text that can be read with the fingers?"),
        default=False,
    )
    TACTICLE_PAVING_CHOICES = [
        ("no", _("No")),
        ("yes", _("Yes")),
        ("partial", _("Partial")),
        ("incorrect", _("Incorrect")),
    ]
    tactile_paving = models.CharField(
        _("Tactile paving"),
        help_text=_("Is there a textured ground surface to guide you?"),
        max_length=10,
        choices=TACTICLE_PAVING_CHOICES,
        default="no",
    )
    coordinates = PointField(
        _("geo coordinates"),
        help_text=_("GPS coordinates of the location."),
        blank=True,
        null=True,
        srid=4326,
    )
    osm_id = models.CharField(
        _("osm id"),
        help_text=_("Id of the object in the osm, that is related to this location."),
        blank=True,
        max_length=20,
    )
    OSM_TYPE_CHOICES = [
        ("node", _("Node")),
        ("way", _("Way")),
        ("relation", _("Relation")),
    ]
    osm_type = models.CharField(
        _("osm type"),
        help_text=_("Type of the object in the osm, that is related to this location."),
        choices=OSM_TYPE_CHOICES,
        default="node",
        blank=True,
        max_length=50,
    )

    class Meta:  # noqa: D106
        verbose_name = _("location")
        verbose_name_plural = _("locations")

    def __str__(self):  # noqa: D105
        return self.address()

    def address(self) -> str:  # noqa: D102
        return f"{self.name + ',' if self.name else ''} {self.street_address}, {self.postal_code} {self.address_region}"

    def get_geo_link(self) -> str:
        """Return a geo: link with the location."""
        if self.coordinates:
            return f"geo:{self.coordinates.y},{self.coordinates.x}"
        return ""

    @classmethod
    def extract_boolean(cls, key: str, value: str) -> bool:
        """Return a boolean representation of the string."""
        if value == "yes":
            return True
        if value == "no":
            return False
        raise Exception(f"'{value}' is not a valid boolean option.")

    @classmethod
    def extract_wheelchair(cls, key: str, value: str) -> str:
        """Return wheelchair option if it is a known one."""
        for choice in cls.ACCESSIBILITY_CHOICES:
            if choice[0] == value:
                return value
        raise Exception(f"'{value}' is not a valid wheelchair accessibility option.")

    @classmethod
    def extract_tactile_paving(cls, key: str, value: str) -> str:
        """Return tactile_paving option if it is a known one."""
        for choice in cls.TACTICLE_PAVING_CHOICES:
            if choice[0] == value:
                return value
        raise Exception(f"'{value}' is not a valid tactile paving option.")

    @classmethod
    def extract_language_description(cls, key: str, value: str) -> str:
        """Return the description in the current active language or the untranslated default."""
        lang_match = re.match(".*:([a-z]{2})$", key)
        lang_is_current_lang = lang_match and lang_match.group(1) == get_language()
        default_lang = not lang_match
        if lang_is_current_lang or default_lang:
            return value
        raise Exception("Not a translation in one of the activated languages.")

    @classmethod
    def osm_extractor(cls, osm_tags: dict) -> dict:
        """Return data from osm tags, that can be used as initial data for a PhysicalLocation."""
        tactile_writing_regex = re.compile(
            (
                "(^tactile_writing"
                "(:(braille|embossed_printed_letters|embossed_letters|engraved_letters))?"
                "(:[a-z]{2})?)"
            )
        )
        wheelchair_description_regex = re.compile(
            "(^wheelchair:description(:[a-z]{2})?)"
        )
        blind_description_regex = re.compile("(^blind:description(:[a-z]{2})?)")

        extracted_data = {}

        for osm_key in osm_tags.keys():
            match osm_key:
                case "wheelchair":
                    key = "wheelchair_accessible"
                    extractor = cls.extract_wheelchair

                case "toilets:wheelchair":
                    key = "wheelchair_accessible_toilet"
                    extractor = cls.extract_boolean

                case "tactile_paving":
                    key = "tactile_paving"
                    extractor = cls.extract_tactile_paving

                case tw if re.match(
                    tactile_writing_regex, tw
                ) and not extracted_data.get("tactile_writing"):
                    key = "tactile_writing"
                    extractor = cls.extract_boolean

                case wd if re.match(wheelchair_description_regex, wd):
                    key = "wheelchair_accessible_description"
                    extractor = cls.extract_language_description

                case bd if re.match(blind_description_regex, bd):
                    key = "blind_description"
                    extractor = cls.extract_language_description

                case _:
                    continue

            try:
                extracted_data[key] = extractor(osm_key, osm_tags[osm_key])
            except Exception:
                print("No valid tag. Checking for the next one if available.")

        return extracted_data


class AreaOfRelevance(MPTTModel, BaseModel):
    """An area that is used as the smallest area for filtering.

    This includes entities likes districts, heighborhoods.
    """

    parent = TreeForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="children"
    )
    name = models.CharField(_("name"), max_length=200)

    class Meta:  # noqa: D106
        verbose_name = _("area of relevance")
        verbose_name_plural = _("areas of relevance")
        ordering = ["name"]

    class MPTTMeta:  # noqa: D106
        order_insertion_by = ["name"]

    def __str__(self):
        return self.name
