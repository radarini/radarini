from django.urls import path
from . import views

app_name = "locations"

urlpatterns = [
    path("search/", views.LocationSearchResults.as_view()),
    path("success/", views.PhysicalLocationSuccessView.as_view()),
    path("add/", views.PhysicalLocationCreateView.as_view()),
    path("<int:pk>/", views.PhysicalLocationUpdateView.as_view()),
    path(
        "<int:pk>/delete/",
        views.PhysicalLocationDeleteView.as_view(),
        name="physicallocations_delete",
    ),
]
