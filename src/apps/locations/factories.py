# noqa: D100
import factory
from factory.fuzzy import FuzzyChoice
from faker.providers import BaseProvider
from django.conf import settings
from django.contrib.gis.geos import Point
from apps.locations.models import PhysicalLocation


class DjangoGeoPointProvider(BaseProvider):
    """Provider of Point objects at random coordinates near the default coordinates from the site settings."""

    def geo_point(self, **kwargs):  # noqa: D102
        faker = factory.faker.faker.Faker()
        return Point(
            x=float(
                faker.coordinate(center=settings.FACTORY_COORDINATES_LNG, radius=0.1)
            ),
            y=float(
                faker.coordinate(center=settings.FACTORY_COORDINATES_LAT, radius=0.1)
            ),
            srid=settings.FACTORY_SRID,
        )


class PhysicalLocationFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake physicallocation entities."""

    class Meta:  # noqa: D106
        model = PhysicalLocation

    factory.Faker.add_provider(DjangoGeoPointProvider)

    street_address = factory.Faker("street_address", locale=settings.FACTORY_LOCALE)
    postal_code = factory.Faker("postcode", locale=settings.FACTORY_LOCALE)
    address_region = factory.Faker("city", locale=settings.FACTORY_LOCALE)
    name = factory.Faker("street_name", locale=settings.FACTORY_LOCALE)
    wheelchair_accessible = FuzzyChoice(
        PhysicalLocation.ACCESSIBILITY_CHOICES, getter=lambda c: c[0]
    )
    wheelchair_accessible_toilet = factory.Faker("pybool")
    wheelchair_accessible_description = factory.Faker(
        "paragraph", nb_sentences=2, locale=settings.FACTORY_LOCALE
    )
    blind_description = factory.Faker(
        "paragraph", nb_sentences=2, locale=settings.FACTORY_LOCALE
    )
    tactile_writing = factory.Faker("pybool")
    tactile_paving = FuzzyChoice(
        PhysicalLocation.TACTICLE_PAVING_CHOICES, getter=lambda c: c[0]
    )
    coordinates = factory.Faker("geo_point")
    osm_id = factory.Faker("random_number")
    osm_type = FuzzyChoice(PhysicalLocation.OSM_TYPE_CHOICES)
