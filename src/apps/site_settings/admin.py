from django.contrib import admin
from parler.admin import TranslatableAdmin

from .models import SettingsTranslation

admin.site.register(SettingsTranslation, TranslatableAdmin)
