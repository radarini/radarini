# noqa: D100
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import SimpleLazyObject
from django.conf import settings
from apps.site_settings.models import SettingsTranslation


def site_settings_processor(request):  # noqa: D103
    site = SimpleLazyObject(lambda: get_current_site(request))
    if not SettingsTranslation.objects.exists():
        SettingsTranslation.objects.create(site=site)
    host_name = settings.HOST

    return {"site": site, "host_name": host_name}
