# noqa: D100
import factory
from django.conf import settings
from django.contrib.sites.models import Site


class SettingsTranslationFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake settings translation entities."""

    class Meta:  # noqa: D106
        model = "site_settings.SettingsTranslation"

    site = Site.objects.first()
    title = "Title"
    subtitle = "Subtitle"
    meta_keywords = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)
    meta_description = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)
