from django.apps import AppConfig
from django.db.models.signals import post_save


class CoreConfig(AppConfig):  # noqa: D101
    name = "apps.site_settings"
    label = "site_settings"
    default_auto_field = "django.db.models.BigAutoField"

    def ready(self):  # noqa: D102
        # App config must be ready for import to work
        from django.contrib.sites.models import Site
        from .signals import create_settings_translated  # now create the second signal

        post_save.connect(create_settings_translated, sender=Site)
