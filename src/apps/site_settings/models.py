from django.contrib.sites.models import Site
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from parler.fields import TranslatedField


class SettingsBaseModel(models.Model):
    """Base model class.

    Sets some common fields like created_at and updated_at.
    """

    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:  # noqa: D106
        abstract = True


class SettingsTranslation(TranslatableModel, SettingsBaseModel):  # noqa: D101
    site = models.OneToOneField(
        Site,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="settings",
        verbose_name="site",
    )

    title = TranslatedField(any_language=True)
    subtitle = TranslatedField(any_language=True)
    meta_keywords = TranslatedField(any_language=True)
    meta_description = TranslatedField(any_language=True)
    default_coordinates = models.CharField(
        _("Instance default coordinates"),
        max_length=50,
        default="52.520008, 13.404954",
        help_text=_("Coordinates for the center of the default map view"),
    )
    default_zoom = models.IntegerField(
        _("Instance default zoom"),
        default=11,
        help_text=_("Zoom level of the default map view"),
    )
    region = models.CharField(
        _("Region"),
        max_length=50,
        default="Berlin",
        help_text=_(
            "The geographic region this instance is deployed in. "
            "(Will be added to OSM search to improve accuracy.)"
        ),
    )
    country_code = models.CharField(
        _("Country code"),
        max_length=2,
        default="DE",
        help_text=_(
            "The country code of the geographic region this instance is deployed in. "
            "(Restricts the OSM search results to locations inside this county.)"
        ),
    )

    translations = TranslatedFields(
        title=models.CharField(_("site title"), max_length=50, default="Radarini"),
        subtitle=models.CharField(
            _("site subtitle"), max_length=500, default="Site subtitle"
        ),
        meta_keywords=models.CharField(
            _("meta keywords"),
            max_length=500,
            default="Direct help, Mutual help, Advice, Sleeping place, "
            "Exchange, Regulars' table, Workshop, Lecture, Filterable, "
            "Searchable, Solidary, City, Groups, Initiatives, "
            "Organizations, Support",
        ),
        meta_description=models.CharField(
            _("meta description"),
            max_length=500,
            default="A filterable and searchable collection of direct and "
            "mutual aid initiatives.",
        ),
    )

    class Meta:  # noqa
        verbose_name = _("Website Setting")
        verbose_name_plural = _("Website Settings")

    def __str__(self):
        return self.site.name
