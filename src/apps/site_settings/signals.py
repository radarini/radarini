from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import SettingsTranslation


@receiver(post_save, sender=Site)
def create_settings_translated(sender, instance, **kwargs):
    """Signal that creates/updates a SettingsTranslation object.

    Triggered after creating/updating a Site object.
    """
    settingsTranslation, created = SettingsTranslation.objects.update_or_create(
        site=instance
    )

    if not created:
        settingsTranslation.save()
