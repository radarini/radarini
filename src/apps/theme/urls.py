from django.urls import path
from django.views.generic.base import TemplateView

app_name = "theme"

urlpatterns = [
    path(
        "language_picker.html",
        TemplateView.as_view(
            template_name="partials/language_picker.html",
            content_type="text/html; charset=utf-8",
        ),
    ),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path(
        "site.webmanifest",
        TemplateView.as_view(
            template_name="site.webmanifest", content_type="application/manifest+json"
        ),
    ),
]
