"""Django tag to make git hash available in templates."""

import os
from django import template

register = template.Library()


@register.simple_tag
def git_hash():
    """Return git hash from environment."""
    return os.environ.get("GIT_HASH")
