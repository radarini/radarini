from django.apps import AppConfig


class ThemeConfig(AppConfig):  # noqa: D101
    name = "apps.theme"
