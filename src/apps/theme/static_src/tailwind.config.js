/**
 * This is a minimal config.
 *
 * If you need the full config, get it from here:
 * https://unpkg.com/browse/tailwindcss@latest/stubs/defaultConfig.stub.js
 */

module.exports = {
    content: [
        /**
         * HTML. Paths to Django template files that will contain Tailwind CSS classes.
         */

        /*  Templates within theme app (<tailwind_app_name>/templates), e.g. base.html. */
        '../templates/**/*.html',

        /*
         * Main templates directory of the project (BASE_DIR/templates).
         * Adjust the following line to match your project structure.
         */
        '../../templates/**/*.html',

        /*
         * Templates in other django apps (BASE_DIR/<any_app_name>/templates).
         * Adjust the following line to match your project structure.
         */
        '../../**/templates/**/*.html',

        /**
         * JS: If you use Tailwind CSS in JavaScript, uncomment the following lines and make sure
         * patterns match your project structure.
         */
        /* JS 1: Ignore any JavaScript in node_modules folder. */
        // '!../../**/node_modules',
        /* JS 2: Process all JavaScript files in the project. */
        '../../../static/js/*.js',
        'node_modules/preline/dist/*.js',
        /**
         * Python: If you use Tailwind CSS classes in Python, uncomment the following line
         * and make sure the pattern below matches your project structure.
         */
        '../../**/*.py'
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    900: '#273bb7',
                    800: '#3450d1',
                    600: '#3170ED',
                    400: '#6992DD',
                    200: '#92ACDB',
                    100: '#a2bdf2'
                },
                secondary: {
                    900: '#AC2894',
                    800: '#C420D9',
                    600: '#D65AE6',
                    400: '#D987E1',
                    200: '#DDA4E2',
                }
            },
            fontFamily: {
                sans: ['Arial', 'sans-serif'],
            },
        },


    },
    plugins: [
        // '@tailwindcss/forms' provides a minimal styling for forms.
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
        require('@tailwindcss/line-clamp'),
        require('@tailwindcss/aspect-ratio'),
        require('tailwindcss-flip'),
        require('daisyui'),
        require('preline/plugin'),
    ],
    daisyui: {
        themes: [
            {
                light: {
                    ...require("daisyui/src/theming/themes")["light"],
                    primary: "#3450d1",
                    secondary: "#c420d9",

                },
            },
            "dark",
        ],
        styled: true,
        base: false,
        darkTheme: "dark",
        utils: true,
        logs: false,
        rtl: false,
        prefix: "",
    },
    darkMode: 'class',
}
