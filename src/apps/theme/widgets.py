from django.forms import models
from django.forms.widgets import SelectMultiple, TextInput
from django_filters import ModelMultipleChoiceFilter
from django_filters.filters import ModelMultipleChoiceField


class ModelChoiceIteratorWithDescriptions(models.ModelChoiceIterator):
    """Iterator including the description in the choice tupels."""

    def __iter__(self):  # noqa: D105
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label, "")
        queryset = self.queryset
        # Can't use iterator() when queryset uses prefetch_related()
        if not queryset._prefetch_related_lookups:
            queryset = queryset.iterator()
        for obj in queryset:
            yield self.choice(obj)

    def choice(self, obj):  # noqa: D102
        (value, label) = super().choice(obj)
        return (
            value,
            label,
            obj.safe_translation_getter("description", any_language=True),
        )


class ModelMultipleChoiceFieldWithDescriptions(ModelMultipleChoiceField):
    """MultiChoice field that includes descriptions for the options."""

    iterator = ModelChoiceIteratorWithDescriptions


class ModelMultipleChoiceFilterWithDescriptions(ModelMultipleChoiceFilter):
    """MultiChoice filter that includes descriptions for the options."""

    field_class = ModelMultipleChoiceFieldWithDescriptions


class Search(TextInput):
    """Text input widget for search."""

    template_name = "partials/search.html"


class AdvancedSelectWithDescriptions(SelectMultiple):
    """Multiselect widget with descriptions, based on the preline 'Advanced Select' component."""

    template_name = "partials/advanced_select.html"

    def optgroups(self, name, value, attrs=None):
        """Return a list of optgroups for this widget."""
        groups = []
        has_selected = False

        for index, (option_value, option_label, description) in enumerate(self.choices):
            if option_value is None:
                option_value = ""

            subgroup = []
            if isinstance(option_label, (list, tuple)):
                group_name = option_value
                subindex = 0
                choices = option_label
            else:
                group_name = None
                subindex = None
                choices = [(option_value, option_label)]
            groups.append((group_name, subgroup, index))

            for subvalue, sublabel in choices:
                selected = (not has_selected or self.allow_multiple_selected) and str(
                    subvalue
                ) in value
                has_selected |= selected
                subgroup.append(
                    self.create_option(
                        name,
                        subvalue,
                        sublabel,
                        description,
                        selected,
                        index,
                        subindex=subindex,
                        attrs=attrs,
                    )
                )
                if subindex is not None:
                    subindex += 1
        return groups

    def get_context(self, name, value, attrs):  # noqa: D102
        context = super().get_context(name, value, attrs)
        if self.allow_multiple_selected:
            context["widget"]["attrs"]["multiple"] = True
        return context

    def create_option(  # noqa: D102
        self,
        name,
        value,
        label,
        description,
        selected,
        index,
        subindex=None,
        attrs=None,
    ):
        index = str(index) if subindex is None else "%s_%s" % (index, subindex)
        option_attrs = (
            self.build_attrs(self.attrs, attrs) if self.option_inherits_attrs else {}
        )
        if selected:
            option_attrs.update(self.checked_attribute)
        if "id" in option_attrs:
            option_attrs["id"] = self.id_for_label(option_attrs["id"], index)
        return {
            "name": name,
            "value": value,
            "label": label,
            "description": description,
            "selected": selected,
            "index": index,
            "attrs": option_attrs,
            "type": self.input_type,
            "template_name": self.option_template_name,
            "wrap_label": True,
        }


class AdvancedSelect(SelectMultiple):
    """Multiselect widget, based on the preline 'Advanced Select' component."""

    template_name = "partials/advanced_select.html"
