from django.contrib import admin, messages
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django_fsm import TransitionNotAllowed

from polymorphic.admin import (
    PolymorphicInlineSupportMixin,
    GenericStackedPolymorphicInline,
)
from parler.admin import TranslatableAdmin, TranslatableStackedInline
from django_fsm_log.admin import StateLogInline

from .models import (
    Organization,
    Contact,
    Service,
    EmailContact,
    PhoneContact,
    PlatformContact,
    WebsiteContact,
)


class LocationInline(admin.StackedInline):  # noqa: D101
    model = Service.physicallocations.through
    extra = 1


class ContactInline(GenericStackedPolymorphicInline):  # noqa: D101
    class EmailContactInline(GenericStackedPolymorphicInline.Child):  # noqa: D106
        model = EmailContact

    class PhoneContactInline(GenericStackedPolymorphicInline.Child):  # noqa: D106
        model = PhoneContact

    class PlatformContactInline(GenericStackedPolymorphicInline.Child):  # noqa: D106
        model = PlatformContact

    class WebsiteContactInline(GenericStackedPolymorphicInline.Child):  # noqa: D106
        model = WebsiteContact

    model = Contact
    child_inlines = (
        EmailContactInline,
        PhoneContactInline,
        WebsiteContactInline,
        PlatformContactInline,
    )


class ServiceInline(TranslatableStackedInline, admin.StackedInline):  # noqa: D101
    model = Service
    readonly_fields = ("edit_link",)

    # Add a link to the edit page for easier editing of contacts
    def edit_link(self, instance):  # noqa: D102
        url = reverse(
            "admin:%s_%s_change"
            % (instance._meta.app_label, instance._meta.model_name),
            args=[instance.pk],
        )
        if instance.pk:
            return mark_safe(
                '<a href="{u}">{link_text}</a>'.format(u=url, link_text=_("edit"))
            )
        else:
            return ""


class OrganizationAdmin(
    TranslatableAdmin, PolymorphicInlineSupportMixin, admin.ModelAdmin
):
    """Admin view for organizations."""

    inlines = (ContactInline, ServiceInline, StateLogInline)
    exclude = (
        "contacts",
        "state",
    )
    list_display = (
        "name",
        "state",
    )
    actions = (
        "make_in_review",
        "make_pending",
        "make_published",
        "make_rejected",
    )

    def get_prepopulated_fields(self, request, obj=None):  # noqa: D102
        # official django-parler workaround
        return {"slug": ("name",)}

    @admin.action(description=_("Review selected organizations"))
    def make_in_review(self, request, queryset):
        """Set organization state to 'in_review' when it is evaluated by a moderator."""
        for obj in queryset:
            try:
                obj.review()
                obj.save()
                messages.success(
                    request,
                    _('Organization %(org_name)s was marked as "in review".')
                    % {"org_name": obj.name},
                )
            except TransitionNotAllowed:
                messages.error(
                    request,
                    _('Organization %(org_name)s could not be marked as "in review".')
                    % {"org_name": obj.name},
                )

    @admin.action(description=_("Mark selected organizations as pending"))
    def make_pending(self, request, queryset):
        """Set organization state to 'pending' when waiting for organization to provide more information or approve."""
        for obj in queryset:
            try:
                obj.pending()
                obj.save()
                messages.success(
                    request,
                    _('Organization %(org_name)s was marked as "pending".')
                    % {"org_name": obj.name},
                )
            except TransitionNotAllowed:
                messages.error(
                    request,
                    _('Organization %(org_name)s could not be marked as "pending".')
                    % {"org_name": obj.name},
                )

    @admin.action(description=_("Reject selected organizations"))
    def make_rejected(self, request, queryset):
        """Set organization state to 'rejected' when it does not fit the topic/requirementsof the instance."""
        for obj in queryset:
            try:
                obj.reject()
                obj.save()
                messages.success(
                    request,
                    _("Organization %(org_name)s was rejected")
                    % {"org_name": obj.name},
                )
            except TransitionNotAllowed:
                messages.error(
                    request,
                    _("Organization %(org_name)s could not be rejected.")
                    % {"org_name": obj.name},
                )

    @admin.action(description=_("Publish selected organizations"))
    def make_published(self, request, queryset):
        """Set organization state to 'published' when it is ready to be published."""
        for obj in queryset:
            try:
                obj.approve()
                obj.save()
                messages.success(
                    request,
                    _("Organization %(org_name)s was published.")
                    % {"org_name": obj.name},
                )
            except TransitionNotAllowed:
                messages.error(
                    request,
                    _("Organization %(org_name)s could not be published.")
                    % {"org_name": obj.name},
                )


class ServiceAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):  # noqa: D101
    inlines = (LocationInline,)
    exclude = ("physicallocations",)


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Service, ServiceAdmin)
