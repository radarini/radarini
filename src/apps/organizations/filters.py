from django.utils.translation import gettext_lazy as _
from django.db.models import Q

import django_filters

from apps.filtercategories.models import (
    ClassificationCategory,
    FeatureCategory,
    ServiceCategory,
    TopicCategory,
    LanguageCategory,
)
from apps.locations.models import AreaOfRelevance
from apps.organizations.models import Service
from apps.theme.widgets import (
    ModelMultipleChoiceFilterWithDescriptions,
    AdvancedSelectWithDescriptions,
    Search,
    AdvancedSelect,
)


class ServiceFilter(django_filters.FilterSet):
    """Multiple choice filter for services."""

    service_category = ModelMultipleChoiceFilterWithDescriptions(
        queryset=ServiceCategory.objects.all(),
        widget=AdvancedSelectWithDescriptions,
        label=_("Services"),
        method="filter_mptt_multiple_categories",
    )
    organization__topics = ModelMultipleChoiceFilterWithDescriptions(
        queryset=TopicCategory.objects.all(),
        widget=AdvancedSelectWithDescriptions,
        label=_("Topics"),
    )
    features = ModelMultipleChoiceFilterWithDescriptions(
        queryset=FeatureCategory.objects.all(),
        widget=AdvancedSelectWithDescriptions,
        conjoined=True,
        label=_("Accessibility"),
    )
    languages = django_filters.ModelMultipleChoiceFilter(
        queryset=LanguageCategory.objects.all(),
        widget=AdvancedSelect,
        conjoined=True,
    )
    classifications = django_filters.ModelMultipleChoiceFilter(
        queryset=ClassificationCategory.objects.all(),
        widget=AdvancedSelect,
        conjoined=True,
    )
    recurring_days = django_filters.MultipleChoiceFilter(
        choices=(
            (0, _("Mo")),
            (1, _("Tu")),
            (2, _("We")),
            (3, _("Th")),
            (4, _("Fr")),
            (5, _("Sa")),
            (6, _("Su")),
        ),
        label=_("Day"),
        widget=AdvancedSelect,
        method="filter_recurring_days",
    )

    text_search = django_filters.CharFilter(
        label=_("Search"),
        method="filter_text_search",
        widget=Search,
    )

    areas_of_relevance = django_filters.ModelMultipleChoiceFilter(
        queryset=AreaOfRelevance.objects.all(),
        widget=AdvancedSelect,
        method="filter_mptt_multiple_categories",
        label=_("Location"),
    )

    class Meta:  # noqa: D106
        model = Service
        fields = [
            "text_search",
            "service_category",
            "areas_of_relevance",
            "recurring_days",
            "languages",
            "features",
            "organization__topics",
            "classifications",
        ]

    def filter_mptt_multiple_categories(self, queryset, name, value):
        """Includes subcategories in filters for multiple choice field."""
        try:
            if value:
                categories = []
                for category in value:
                    categories += category.get_descendants(include_self=True)
                queryset = queryset.filter(**{name + "__in": categories}).distinct()
        except ValueError:
            pass
        return queryset

    def filter_recurring_days(self, queryset, name, value):
        """Custom filter method for recurring days."""  # noqa: D401
        days_to_fields = [
            "recurring_mondays",
            "recurring_tuesdays",
            "recurring_wednesdays",
            "recurring_thursdays",
            "recurring_fridays",
            "recurring_saturdays",
            "recurring_sundays",
        ]

        try:
            if value:
                q_objects = Q()
                for day in value:
                    q_objects |= Q(**{days_to_fields[int(day)]: True})
                queryset = queryset.filter(q_objects)
        except ValueError:
            pass
        return queryset

    def filter_text_search(self, queryset, name, value):
        """Custom filter method for text search on Organization."""  # noqa: D401
        return queryset.filter(
            Q(organization__translations__name__icontains=value)
            | Q(organization__translations__description__icontains=value)
            | Q(translations__description__icontains=value)
            | Q(service_category__translations__name__icontains=value)
            | Q(service_category__translations__description__icontains=value)
        ).distinct()
