from django.db import models
from django.urls.base import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.core import validators
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from polymorphic.models import PolymorphicModel
from parler.models import TranslatableModel, TranslatedFields, TranslatedField
from mptt.models import TreeForeignKey, TreeManyToManyField
from django_fsm import FSMField, transition
from urllib.parse import urlparse

from apps.filtercategories.models import (
    ClassificationCategory,
    FeatureCategory,
    ServiceCategory,
    TopicCategory,
    LanguageCategory,
)
from apps.locations.models import AreaOfRelevance, PhysicalLocation
from apps.organizations.validators import ImageSizeValidator
from apps.organizations.send_mail_async import send_mail_async


class BaseModel(models.Model):
    """Base model class.

    Sets some common fields like created_at and updated_at.
    """

    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:  # noqa: D106
        abstract = True


class Contact(PolymorphicModel, BaseModel):  # noqa: DJ08
    """A polymorphic model with child models.

    Used for contacting an organization/service.
    """

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    class Meta:  # noqa: D106
        verbose_name = _("contact")
        verbose_name_plural = _("contacts")

    def classname(self):  # noqa: D102
        return self.__class__.__name__


class PhoneContact(Contact):
    """A phone contact."""

    phone_regex = validators.RegexValidator(
        regex=r"^\+?1?\d{9,15}$",
        message=_(
            "Phone number must be entered in the format: '+xxxxxxxxxx'. Up to 15 digits allowed."
        ),
    )
    telephone = models.CharField(
        _("telephone number"), max_length=50, validators=[phone_regex]
    )

    class Service(models.TextChoices):  # noqa: D106
        SIGNAL = "Signal"
        TELEGRAM = "Telegram"
        WHATSAPP = "WhatsApp"

    service = models.CharField(
        _("service"), max_length=8, choices=Service.choices, blank=True
    )

    def get_url(self):  # noqa: D102
        if self.service == "Signal":
            return f"https://signal.me/#p/{self.telephone}"
        elif self.service == "Telegram":
            return f"https://telegram.me/{self.telephone}"
        elif self.service == "WhatsApp":
            return f"https://wa.me/{self.telephone}"
        else:
            return f"tel:{self.telephone}"

    class Meta:  # noqa: D106
        verbose_name = _("telephone contact")
        verbose_name_plural = _("telephone contacts")

    def __str__(self):
        if self.service:
            return self.service
        else:
            return self.telephone


class WebsiteContact(Contact):
    """A website contact."""

    website = models.CharField(
        _("website"),
        max_length=250,
        validators=[
            validators.URLValidator(
                schemes=["http", "https"],
                message=_(
                    "Could not read your input as valid URL address. Did you include https://?"
                ),
            )
        ],
    )

    class Meta:  # noqa: D106
        verbose_name = _("website contact")
        verbose_name_plural = _("website contacts")

    def __str__(self):
        return self.website

    def get_host_name(self):  # noqa: D102
        return urlparse(self.website).netloc


class EmailContact(Contact):
    """An email contact."""

    email = models.EmailField(
        _("e-mail address"), max_length=250, validators=[validators.validate_email]
    )

    class Meta:  # noqa: D106
        verbose_name = _("email contact")
        verbose_name_plural = _("email contacts")

    def __str__(self):
        return self.email


class PlatformContact(Contact):
    """A social media platform contact."""

    class Meta:  # noqa: D106
        verbose_name = _("platform contact")
        verbose_name_plural = _("platform contacts")

    class Platform(models.TextChoices):  # noqa: D106
        FACEBOOK = "FB"
        TWITTER = "TW"
        INSTAGRAM = "IG"
        MASTODON = "MS"

    platform = models.CharField(_("platform"), max_length=2, choices=Platform.choices)
    profile_name = models.CharField(_("profile name"), max_length=250)

    def __str__(self):
        return f"{self.platform}: {self.profile_name}"

    def get_url(self):  # noqa: D102
        if self.platform == "FB":
            return f"https://facebook.com/{self.profile_name}"
        elif self.platform == "TW":
            return f"https://twitter.com/{self.profile_name}"
        elif self.platform == "IG":
            return f"https://instagram.com/{self.profile_name}"
        else:
            return self.profile_name


class Organization(TranslatableModel, BaseModel):
    """The main model of this application."""

    # translatable fields are added via django-parler
    name = TranslatedField(any_language=True)
    alternate_name = TranslatedField(any_language=True)
    description = TranslatedField(any_language=True)
    participation = TranslatedField(any_language=True)
    superorganization = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        limit_choices_to={"state": "approved"},
        blank=True,
        null=True,
        related_name="suborganization",
    )
    translations = TranslatedFields(
        name=models.CharField(
            _("name"), max_length=100, help_text=_("The name of the organization.")
        ),
        alternate_name=models.CharField(
            _("alternative name"),
            max_length=100,
            blank=True,
            help_text=_("An alternative name for the organization or an abbreviation."),
        ),
        description=models.TextField(
            _("description"),
            help_text=_(
                "The general description of the organization. What they do and"
                " with what motivation and specifics. These contents also "
                "appear in the free text search. Please do not enter detailed "
                "time information. The information might get outdated. The visitors"
                " should always need to check your website. (~500 characters)"
            ),
        ),
        participation=models.TextField(
            _("participation"),
            blank=True,
            help_text=_(
                "A description of the possibilities to support and participate in the organization. "
                "What are the conditions? What is currently needed?"
            ),
        ),
    )
    slug = models.SlugField(
        _("slug"),
        max_length=100,
        null=False,
        unique=True,
        help_text=_("The string used in the url. Will be generated automaticaly."),
    )
    topics = TreeManyToManyField(
        TopicCategory,
        help_text=_(
            "The topics that an organization is working on. Always try to "
            "specify nested topics as precisely as possible. "
            "(<a class='underline' href='/filters/topics' target='_blank'>More information</a>)"
        ),
    )
    logo = models.ImageField(
        _("logo"),
        upload_to="organizations/%Y/%m/%d/",
        null=True,
        blank=True,
        validators=[ImageSizeValidator(settings.MAX_LOGO_UPLOAD_SIZE)],
        help_text=_(
            "Image that will be displayed for the organization. (Allowed formats: png, jpeg, jpg)"
        ),
    )
    contacts = GenericRelation(Contact)
    verified = models.BooleanField(_("verified"), default=False)
    reviewed_at = models.DateTimeField(_("reviewed at"), null=True, blank=True)
    comment = models.TextField(_("comment"), blank=True)
    promotion_material = models.BooleanField(
        _("promotion material"),
        help_text=_(
            "Does the admin collective have promotion material of that organization?"
        ),
        default=False,
    )
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="created_organizations",
    )
    assignees = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="assigned_organizations"
    )
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="own_organizations"
    )
    state = FSMField(default="new")

    class Meta:  # noqa: D106
        verbose_name = _("organization")
        verbose_name_plural = _("organizations")

    def __str__(self):
        return self.name

    def get_absolute_url(self):  # noqa: D102
        return reverse("organizations:show", kwargs={"slug": self.slug})

    def can_update(self, user):  # noqa: D102
        return user.is_superuser or user in self.assignees.all()

    @transition(field=state, source="+", target="approved")
    def approve(self):  # noqa: D102
        # Inform the organization and assignees about the publication via email
        assignees_email_contacts = [user.email for user in self.assignees.all()]
        organization_email_contacts = [
            e.email for e in self.contacts.instance_of(EmailContact).all()
        ]
        recipients = [*assignees_email_contacts, *organization_email_contacts]
        if recipients:
            site = Site.objects.get_current()
            title = (
                f"[{settings.HOST}] {self.name} wurde veröffentlicht / was published!"
            )
            msg = render_to_string(
                "organizations/partials/orga_publication_email.html",
                {
                    "organization": self,
                    "site": site,
                    "support_mail": settings.SUPPORT_MAIL,
                    "host_name": settings.HOST,
                },
            )

            send_mail_async(
                title,
                msg,
                settings.DEFAULT_FROM_EMAIL,
                recipients,
            )

    @transition(field=state, source="+", target="rejected")
    def reject(self):  # noqa: D102
        pass

    @transition(field=state, source="+", target="in_review")
    def review(self):  # noqa: D102
        pass

    @transition(field=state, source="+", target="pending")
    def pending(self):  # noqa: D102
        pass

    @transition(field=state, source="*", target="new")
    def state_init(self):  # noqa: D102
        pass


class Service(TranslatableModel, BaseModel):
    """Different services offered by an Organization."""

    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, related_name="services"
    )
    service_category = TreeForeignKey(
        ServiceCategory,
        on_delete=models.CASCADE,
        related_name="services",
        help_text=_(
            "Each service of an organization has a service category. "
            "This is the finest filterable description of a service. "
            "(<a class='underline' href='/filters/services' target='_blank'>More information</a>)"
        ),
    )
    features = models.ManyToManyField(
        FeatureCategory,
        related_name="services",
        blank=True,
        help_text=_(
            "Which features apply to the service? (<a class='underline' href='/filters/features' target='_blank'>More information</a>)"
        ),
    )
    languages = models.ManyToManyField(
        LanguageCategory,
        blank=True,
        related_name="services",
        help_text=_(
            "Select the offered language(s) of the service. "
            "If a language is missing <a href='/contact' target='_blank' class='underline'>contact us</a>."
        ),
    )
    classifications = models.ManyToManyField(
        ClassificationCategory,
        related_name="services",
        blank=True,
        help_text=_(
            "Which political classifications apply to the service? "
            "(<a class='underline' href='/filters/classifications' target='_blank'>More information</a>)"
        ),
    )
    description = TranslatedField(any_language=True)
    translations = TranslatedFields(
        description=models.TextField(
            _("description"),
            blank=True,
            help_text=_(
                "The name of the service, as it is held by the organization, "
                "or a short meaningful description of the service. "
                "If applicable include exact target group or specific topics."
            ),
        )
    )

    recurring_mondays = models.BooleanField(_("Recurrs on Mondays"), default=False)
    recurring_tuesdays = models.BooleanField(_("Recurrs on Tuesdays"), default=False)
    recurring_wednesdays = models.BooleanField(
        _("Recurrs on Wednesdays"), default=False
    )
    recurring_thursdays = models.BooleanField(_("Recurrs on Thursdays"), default=False)
    recurring_fridays = models.BooleanField(_("Recurrs on Fridays"), default=False)
    recurring_saturdays = models.BooleanField(_("Recurrs on Saturdays"), default=False)
    recurring_sundays = models.BooleanField(_("Recurrs on Sundays"), default=False)
    recurring_weekly = models.BooleanField(_("Happens every week"), default=False)
    physicallocations = models.ManyToManyField(
        PhysicalLocation, related_name="services", blank=True
    )
    areas_of_relevance = TreeManyToManyField(
        AreaOfRelevance, related_name="services", blank=True
    )

    class Meta:  # noqa: D106
        verbose_name = _("service")
        verbose_name_plural = _("services")

    def __str__(self):
        return f"{self.organization}: {self.service_category}"

    def get_list_of_days(self):  # noqa: D102
        days = []
        if self.recurring_mondays:
            days.append(_("Monday"))
        if self.recurring_tuesdays:
            days.append(_("Tuesday"))
        if self.recurring_wednesdays:
            days.append(_("Wednesday"))
        if self.recurring_thursdays:
            days.append(_("Thursday"))
        if self.recurring_fridays:
            days.append(_("Friday"))
        if self.recurring_saturdays:
            days.append(_("Saturday"))
        if self.recurring_sundays:
            days.append(_("Sunday"))

        return days


class ShareKey(models.Model):  # noqa: DJ08
    """A key for sharing info via links."""

    location = models.TextField()  # absolute path
    token = models.CharField(max_length=40, primary_key=True)
    expiration_date = models.DateTimeField()

    @property
    def expired(self):  # noqa: D102
        if self.expiration_date < timezone.now():
            return True
        else:
            return False
