from django.contrib.sitemaps import Sitemap  # noqa
from apps.organizations.models import Organization


class OrganizationSitemap(Sitemap):  # noqa
    protocol = "https"

    def items(self):  # noqa
        return Organization.objects.filter(state="approved")
