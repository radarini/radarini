# noqa: D100
class LowerCaseConverter:  # noqa: D101
    regex = "[^/]+"

    def to_python(self, value):  # noqa: D102
        return value.lower()

    def to_url(self, value):  # noqa: D102
        return value.lower()
