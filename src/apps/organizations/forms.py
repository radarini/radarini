from polymorphic.formsets import (
    generic_polymorphic_inlineformset_factory,
    GenericPolymorphicFormSetChild,
    BaseGenericPolymorphicInlineFormSet,
)
from apps.filtercategories.models import (
    TopicCategory,
    LanguageCategory,
    FeatureCategory,
    ClassificationCategory,
)
from apps.locations.models import AreaOfRelevance
from apps.organizations.models import (
    Organization,
    Service,
    Contact,
    PhoneContact,
    WebsiteContact,
    PlatformContact,
    EmailContact,
)
from apps.organizations.widgets import ImageWidget
from apps.organizations.widgets import TreeSelectMultiple
from django.forms import (
    inlineformset_factory,
    ModelMultipleChoiceField,
)
from django.forms.models import BaseInlineFormSet
from django.forms.widgets import CheckboxSelectMultiple, Textarea, TextInput
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify
from parler.forms import TranslatableModelForm
from PIL import Image


class OrganizationForm(TranslatableModelForm):
    """Form for creating an organization."""

    template_name = "organizations/forms/organization_form.html"

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        self.fields["topics"].label = _("Topics")

    class Meta:  # noqa: D106
        model = Organization
        fields = ["name", "alternate_name", "description", "logo", "topics"]
        widgets = {
            "logo": ImageWidget(attrs={"accept": ".png,.jpeg,.jpg"}),
            "topics": TreeSelectMultiple(
                queryset=TopicCategory.objects.all(),
                description_link_name="topics",
            ),
            "description": Textarea(
                attrs={
                    "class": "w-full border-[#d1d5db]",
                    "x-data": "{resize: () => {$el.style.height = '5px'; $el.style.height = $el.scrollHeight + 'px'}}",
                    "x-init": "resize()",
                    "@input": "resize()",
                }
            ),
        }

    def save(self):  # noqa: D102
        organization = super(OrganizationForm, self).save(commit=False)
        if not organization.slug:
            if organization.alternate_name:
                organization.slug = slugify(organization.alternate_name)
            else:
                organization.slug = slugify(organization.name)
            while Organization.objects.filter(slug=organization.slug).exists():
                organization.slug = (
                    organization.slug + "-" + get_random_string(length=3)
                )
        organization.save()
        self.save_m2m()

        if organization.logo:
            img = Image.open(organization.logo.path)
            if img.width > 300 or img.height > 100:
                output_size = (300, 100)
                img.thumbnail(output_size)
                img.save(organization.logo.path)

        return organization


class ServiceForm(TranslatableModelForm):  # noqa: DJ06
    """Form for creating a service."""

    languages = ModelMultipleChoiceField(
        queryset=LanguageCategory.objects.translated().order_by("translations__name"),
        widget=CheckboxSelectMultiple,
        blank=True,
        required=False,
    )
    features = ModelMultipleChoiceField(
        queryset=FeatureCategory.objects.translated().order_by("translations__name"),
        widget=CheckboxSelectMultiple,
        blank=True,
        required=False,
    )
    classifications = ModelMultipleChoiceField(
        queryset=ClassificationCategory.objects.translated().order_by(
            "translations__name"
        ),
        widget=CheckboxSelectMultiple,
        blank=True,
        required=False,
    )

    class Meta:  # noqa: D106
        model = Service
        exclude = ("organization", "created_at")
        widgets = {
            "areas_of_relevance": TreeSelectMultiple(
                queryset=AreaOfRelevance.objects.all(), select_all=True
            ),
            "description": Textarea(
                attrs={
                    "class": "w-full border-[#d1d5db]",
                    "x-data": "{resize: () => {$el.style.height = '5px'; $el.style.height = `${$el.scrollHeight}px`}}",
                    "x-init": "resize(), $watch('open', () => $nextTick(() => resize()))",
                    "@input": "resize()",
                }
            ),
        }

    def save(self, commit=True):  # noqa: D102
        instance = super().save(commit)
        return instance


class BaseServicesFormset(BaseInlineFormSet):  # noqa: D101
    template_name = "organizations/forms/services_formset.html"


ServicesFormSet = inlineformset_factory(
    Organization,
    Service,
    form=ServiceForm,
    formset=BaseServicesFormset,
    extra=1,
    can_delete=False,
)


class BaseContactFormSet(BaseGenericPolymorphicInlineFormSet):  # noqa: D101
    template_name = "organizations/forms/contacts_formset.html"


ContactFormSet = generic_polymorphic_inlineformset_factory(
    Contact,
    formset_children=(
        GenericPolymorphicFormSetChild(
            PhoneContact,
            fields=("telephone", "service"),
            widgets={
                "telephone": TextInput(attrs={"placeholder": "+49123456789"}),
            },
        ),
        GenericPolymorphicFormSetChild(
            PlatformContact, fields=("platform", "profile_name")
        ),
        GenericPolymorphicFormSetChild(
            EmailContact,
            fields=["email"],
            widgets={"email": TextInput(attrs={"placeholder": "mail@your-ini.org"})},
        ),
        GenericPolymorphicFormSetChild(
            WebsiteContact,
            fields=["website"],
            widgets={
                "website": TextInput(attrs={"placeholder": "https://your-ini.org"})
            },
        ),
    ),
    extra=4,
    formset=BaseContactFormSet,
    can_delete=False,
)
