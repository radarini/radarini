# noqa: D100
from django.forms.widgets import SelectMultiple, ClearableFileInput


class TreeSelectMultiple(SelectMultiple):
    """Widget that allows to select multiple elements from a model with a mptt hierarchy."""

    template_name = "forms/widgets/tree_select_multiple.html"
    option_template_name = "forms/widgets/select_option.html"

    def __init__(
        self,
        attrs=None,
        choices=(),
        queryset=None,
        select_all=False,
        description_link_name=None,
    ):
        super().__init__(attrs, choices)
        self.queryset = queryset
        self.select_all = select_all
        self.description_link_name = description_link_name

    def get_context(self, name, value, attrs):  # noqa: D102
        if self.queryset is not None:
            self.queryset = self.queryset.all()

        selected = {}
        if value:
            for v in value:
                selected[v] = {"id": v, "label": self.queryset.get(id=v).name}

        allItems = []
        if self.queryset:
            for node in self.queryset:
                allItems.append({"label": node.name, "id": node.id})

        return {
            "widget": {
                "name": name,
                "value": value,
                "template_name": self.option_template_name,
                "queryset": self.queryset,
                "select_all": self.select_all,
                "description_link_name": self.description_link_name,
                "selected": selected,
                "allItems": allItems,
            }
        }


class TreeSelectMultipleFilters(SelectMultiple):
    """Widget that allows to select multiple elements from a model with a mptt hierarchy."""

    template_name = "forms/widgets/tree_select_multiple_filter.html"
    option_template_name = "forms/widgets/select_option.html"

    def __init__(
        self,
        attrs=None,
        choices=(),
        queryset=None,
        description_link_name=None,
        filter_type=None,
    ):
        super().__init__(attrs, choices)
        self.queryset = queryset
        self.description_link_name = description_link_name
        self.filter_type = filter_type

    def get_context(self, name, value, attrs):  # noqa: D102
        return {
            "widget": {
                "name": name,
                "value": value,
                "template_name": self.option_template_name,
                "queryset": self.queryset,
                "description_link_name": self.description_link_name,
                "filter_type": self.filter_type,
            }
        }


class ImageWidget(ClearableFileInput):  # noqa: D101
    template_name = "forms/widgets/clearable_file_input.html"
