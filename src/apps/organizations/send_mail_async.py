# noqa: D100
from threading import Thread
from django.core.mail import EmailMultiAlternatives


class EmailThread(Thread):
    """An model for sending emails."""

    def __init__(
        self, subject, text_content, from_email, recipient_list, html_content=None
    ):  # noqa: D102
        self.subject = subject
        self.recipient_list = recipient_list
        self.text_content = text_content
        self.html_content = html_content
        self.from_email = from_email
        Thread.__init__(self)

    def run(self):  # noqa: D102
        msg = EmailMultiAlternatives(
            self.subject, self.text_content, self.from_email, self.recipient_list
        )
        if self.html_content is not None:
            msg.attach_alternative(self.html_content, "text/html")
        msg.send(fail_silently=False)


def send_mail_async(  # noqa: D103
    subject,
    text_content,
    from_email,
    recipient_list,
    html_content=None,
):
    EmailThread(subject, text_content, from_email, recipient_list, html_content).start()
