from django.http.response import Http404
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import gettext as _
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import never_cache
from django.views.decorators.vary import vary_on_headers
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.db.models import Count, OuterRef, Subquery
from django.contrib.gis.serializers.geojson import Serializer
from apps.accounts.forms import OptionalSignUpForm
from apps.locations.models import PhysicalLocation
from apps.flatpages_parler.models import ParlerFlatPage
from apps.organizations.filters import ServiceFilter
from apps.organizations.models import Organization, Service, ShareKey, EmailContact
from apps.organizations.forms import OrganizationForm, ContactFormSet, ServicesFormSet
from apps.organizations.create_share import create_share_key
from apps.organizations.wizard_utils import handle_user_creation, handle_email_dispatch

from itertools import groupby
from django_fsm import TransitionNotAllowed
from formtools.wizard.views import SessionWizardView
from django_fsm_log.models import StateLog
from datetime import datetime

import os
import csv
import json


class SharedPage(View):
    """A page shared via a ShareKey."""

    def dispatch(self, request, key, *args, **kwargs):  # noqa: D102
        if ShareKey.objects.filter(pk=key).exists():
            shareKey = ShareKey.objects.get(pk=key)
            if shareKey.expired:
                raise Http404(_("Sharelink expired"))
            func, args, kwargs = resolve(shareKey.location)
            kwargs["shared"] = True
            return func(request, *args, **kwargs)
        else:
            raise Http404(_("Could not find the shared entry"))


class Filter(View):
    """Abstract view to filter the services."""

    container_template = ""
    data_template = ""

    def filter(self, request):
        """Filter the services and group them by organizations."""
        services_list = (
            Service.objects.all()
            .filter(organization__state="approved")
            .order_by("-organization__updated_at", "organization")
            .prefetch_related("organization")
        )

        service_filter = ServiceFilter(request.GET, queryset=services_list)
        group = groupby(service_filter.qs, lambda x: x.organization)
        grouped_services = [(k, list(e)) for (k, e) in group]

        self.context = {
            "filter": service_filter,
            "results": grouped_services,
        }

    def respond(self, request):
        """Render the response to the filter request, depending on the htmx target.

        none: Render the full page, including filter and results in current view
        results-container: Render the container of the current view including the data
        results-data: Only render the results data of current view
        """
        if request.htmx.target == "results-data":
            return render(request, self.data_template, self.context)

        elif request.htmx.target == "results-container":
            return render(
                request,
                self.container_template,
                {**self.context, "data_template": self.data_template},
            )

        else:
            return render(
                request,
                "filter/filter.html",
                {
                    **self.context,
                    "container_template": self.container_template,
                    "data_template": self.data_template,
                },
            )


class List(Filter):
    """List view of the filtered services."""

    container_template = "filter/list_container.html"
    data_template = "filter/list_data.html"

    @vary_on_headers("HX-Request")
    def get(self, request):  # noqa: D102
        super().filter(request)
        paginator = Paginator(self.context["results"], settings.PAGINATION_PER_PAGE)
        page = request.GET.get("page")

        try:
            results = paginator.page(page)
            results.paginator.page_range_elided = (
                results.paginator.get_elided_page_range(page, on_each_side=1, on_ends=1)
            )
        except PageNotAnInteger:
            results = paginator.page(1)
            results.paginator.page_range_elided = (
                results.paginator.get_elided_page_range(1, on_each_side=1, on_ends=1)
            )
        except EmptyPage:
            results = paginator.page(paginator.num_pages)
            results.paginator.page_range_elided = (
                results.paginator.get_elided_page_range(
                    paginator.num_pages, on_each_side=1, on_ends=1
                )
            )

        try:
            welcome_text = ParlerFlatPage.objects.get(url="/welcome/")
        except ObjectDoesNotExist:
            welcome_text = None

        self.context["welcome_text"] = welcome_text
        self.context["page"] = page
        self.context["results"] = results

        return super().respond(request)


class Map(Filter):
    """Map view of the filtered services."""

    container_template = "filter/map_container.html"
    data_template = "filter/map_data.html"

    @vary_on_headers("HX-Request")
    def get(self, request):  # noqa: D102
        super().filter(request)

        location_serializer = Serializer()
        geo_json = location_serializer.serialize(
            PhysicalLocation.objects.filter(
                id__in=self.context["filter"].qs.values("physicallocations")
            ).distinct(),
            geometry_field="coordinates",
            fields=[],
        )
        geo_json = json.loads(geo_json)
        # Add the services ids to the geo_json
        for feature in geo_json["features"]:
            feature["properties"]["services"] = list(
                self.context["filter"]
                .qs.filter(physicallocations=feature["id"])
                .values_list("id", flat=True)
            )
        no_physical_locations_qs = self.context["filter"].qs.filter(
            physicallocations=None
        )
        if no_physical_locations_qs.exists():
            geo_json["non_physical_locations"] = no_physical_locations_qs.count()
        self.context["results"] = geo_json

        return super().respond(request)


class OrganizationDetail(View):
    """Detail view for organization."""

    template_name = "organizations/show.html"

    def get(self, request, slug, *args, **kwargs):  # noqa: D102
        geo_json = None
        try:
            organization = Organization.objects.prefetch_related(
                "contacts",
                "topics",
                "services",
                "services__features",
                "services__classifications",
            ).get(slug__iexact=slug)
            if PhysicalLocation.objects.filter(
                services__organization=organization
            ).exists():
                # Get all locations of services from organization
                location_serializer = Serializer()
                geo_json = location_serializer.serialize(
                    PhysicalLocation.objects.filter(
                        services__organization=organization
                    ).distinct(),
                    geometry_field="coordinates",
                    fields=[],
                )
                geo_json = json.loads(geo_json)
                # Add the services ids to the geo_json
                for feature in geo_json["features"]:
                    feature["properties"]["services"] = list(
                        organization.services.filter(
                            physicallocations=feature["id"]
                        ).values_list("id", flat=True)
                    )
        except Organization.DoesNotExist:
            raise Http404(_("Organization does not exist"))

        if organization.state == "approved":
            return render(
                request,
                self.template_name,
                {
                    "organization": organization,
                    "services_physicallocations": geo_json,
                },
            )
        elif (
            organization.can_update(request.user)
            or kwargs.get("shared", None) is not None
        ):
            context = {
                "organization": organization,
                "services_physicallocations": geo_json,
            } | kwargs
            return render(request, self.template_name, context)
        else:
            raise PermissionDenied

    def post(self, request, slug, *args, **kwargs):  # noqa: D102
        data = request.POST
        if request.user.is_superuser or self.kwargs.get("shared", None):
            org = get_object_or_404(Organization, slug__iexact=slug)
            if data.get("approve"):
                try:
                    org.approve()
                    org.save()
                    messages.success(
                        request,
                        _("'%(org_name)s' was published.") % {"org_name": org.name},
                    )
                    return redirect(request.path)
                except TransitionNotAllowed:
                    messages.error(
                        request,
                        _("'%(org_name)s' could not be published.")
                        % {"org_name": org.name},
                    )
                    return redirect(org)
            elif data.get("account_url"):
                signup_key = create_share_key(
                    url=reverse("accounts:signup", kwargs={"slug": org.slug}),
                    expiration_seconds=60 * 60 * 24 * 7 * 4,  # 4 Weeks
                )
                url = f"https://{settings.HOST}/access/{signup_key.pk}/"
                return render(
                    request, self.template_name, {"organization": org, "reset_url": url}
                )
            elif data.get("preview_url"):
                verify_key = create_share_key(
                    url=org.get_absolute_url(),
                    expiration_seconds=60 * 60 * 24 * 7 * 4,  # 4 Weeks
                )
                url = f"https://{settings.HOST}/access/{verify_key.pk}/"
                return render(
                    request,
                    self.template_name,
                    {"organization": org, "preview_url": url},
                )
            elif data.get("verify"):
                org.verified = True
                org.save()
                return redirect(request.path)

        else:
            raise PermissionDenied


class PromotedOrganizations(View):
    """
    List for promoted organizations, in different formats.

    This only includes organizations that have at least one service,
    that has one or more ClassificationCategories objects.
    There is a PDF and CSV export via the `format` parameter.
    """

    template_name = "organizations/promoted_print.html"

    def get(self, request):  # noqa: D102
        if not request.user.is_staff:
            return redirect(to="accounts:login")

        max_classifications_count_subquery = (
            Service.objects.all()
            .filter(organization=OuterRef("pk"))
            .annotate(classifications_count=Count("classifications"))
            .order_by("-classifications_count")
            .values("classifications_count")[:1]
        )

        promoted_organizations = (
            Organization.objects.all()
            .filter(state="approved")
            .annotate(
                services__classifications_count=Count("services__classifications")
            )
            .filter(services__classifications_count__gt=0)
            .annotate(
                max_num_classifications=Subquery(max_classifications_count_subquery)
            )
            .order_by("-max_num_classifications")
        )

        if request.GET.get("format") == "csv":
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = (
                "attachment; filename={}_{:%Y%m%d}.csv".format(
                    "promoted_organizations", datetime.now()
                )
            )
            writer = csv.writer(response)
            field_names = [
                "name",
                "max_num_classifications",
                "promotion_material",
                "participation",
                "contacts",
                "slug",
            ]
            writer.writerow(field_names)
            for obj in promoted_organizations:
                row = []
                for field in field_names:
                    if field == "contacts":
                        mail_objects = [
                            obj
                            for obj in obj.contacts.all()
                            if isinstance(obj, EmailContact)
                        ]
                        if mail_objects:
                            row.append(mail_objects[0])
                        else:
                            row.append("")
                        continue
                    row.append(getattr(obj, field))
                writer.writerow(row)

            return response

        return render(
            request,
            self.template_name,
            {"organizations": promoted_organizations},
        )


class UpdateAuthorization(View):
    """Base view that infers permission to update model."""

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        self.org = get_object_or_404(
            Organization, slug__iexact=kwargs.get("slug", None)
        )
        if self.org.can_update(request.user):
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied


class OrganizationUpdate(UpdateAuthorization):
    """View to update organization."""

    template_name = "organizations/edit_organization.html"

    def get(self, request, slug):  # noqa: D102
        form = OrganizationForm(instance=self.org)
        return render(
            request,
            self.template_name,
            {"form": form, "organization": self.org},
        )

    def post(self, request, slug):  # noqa: D102
        form = OrganizationForm(request.POST, request.FILES, instance=self.org)
        if form.is_valid():
            new_organization = form.save()
            return redirect(new_organization)
        return render(
            request,
            self.template_name,
            {"form": form, "organization": self.org},
        )


class ContactUpdate(UpdateAuthorization):
    """View to update a contact."""

    template_name = "organizations/create_contact.html"

    def get(self, request, slug):  # noqa: D102
        formset = ContactFormSet(instance=self.org)
        formset.can_delete = True

        return render(
            request,
            self.template_name,
            {"formset": formset, "organization": self.org},
        )

    def post(self, request, slug):  # noqa: D102
        formset = ContactFormSet(request.POST, instance=self.org)
        formset.can_delete = True

        if formset.is_valid():
            formset.save()
            return redirect(self.org)
        return render(
            request, self.template_name, {"formset": formset, "organization": self.org}
        )


class ServiceUpdate(UpdateAuthorization):
    """View to update a service."""

    template_name = "organizations/edit_services.html"

    def get(self, request, slug):  # noqa: D102
        formset = ServicesFormSet(instance=self.org)
        formset.can_delete = True

        return render(
            request, self.template_name, {"formset": formset, "organization": self.org}
        )

    def post(self, request, slug):  # noqa: D102
        formset = ServicesFormSet(request.POST, instance=self.org)
        formset.can_delete = True

        if formset.is_valid():
            formset.save()
            return redirect(self.org)

        return render(
            request, self.template_name, {"formset": formset, "organization": self.org}
        )


# Avoid browser caching of the dashboard
@method_decorator(never_cache, name="dispatch")
class AdminDashboard(View):  # noqa: D101
    model = User
    template_name = "organizations/admin_dashboard.html"

    def get(self, request):  # noqa: D102
        if not request.user.is_staff:
            return redirect(to="accounts:login")

        organizations = list(
            Organization.objects.all()
            .distinct()
            .values(
                "state",
                "verified",
                "id",
                "comment",
                "creator__username",
            )
        )

        # Add latest state change timestamp, translated name and absolute url to each organization
        for organization in organizations:
            org_obj = Organization.objects.get(id=organization["id"])
            last = StateLog.objects.for_(org_obj).last()
            if last:
                organization["latest_state_change"] = last.timestamp.timestamp()
            else:
                organization["latest_state_change"] = None
            organization["url"] = org_obj.get_absolute_url()
            organization["name"] = org_obj.name

        return render(
            request,
            self.template_name,
            {"organizations": organizations},
        )

    def post(self, request):  # noqa: D102
        if not request.user.is_staff:
            return redirect(to="accounts:login")
        organization = Organization.objects.get(id=request.POST.get("organization_id"))
        # Update state
        if state := request.POST.get("state"):
            if state == "approved":
                organization.approve()
            elif state == "rejected":
                organization.reject()
            elif state == "in_review":
                organization.review()
            elif state == "pending":
                organization.pending()
            elif state == "new":
                organization.state_init()
            organization.save()
            if state == "delete":
                organization.delete()
        # Update comment
        comment = request.POST.get("comment")
        if comment is not None:
            organization.comment = comment
            organization.save()
            return HttpResponse(status=204)

        return HttpResponseRedirect(request.path_info)


class OrganzationSubmitWizardView(SessionWizardView):
    """Organsiation submit wizard, includes all associated forms."""

    form_list = (OrganizationForm, ContactFormSet, ServicesFormSet, OptionalSignUpForm)
    template_name = "organizations/submit_wizard.html"
    file_storage = FileSystemStorage(
        location=os.path.join(settings.MEDIA_ROOT, "organizations")
    )

    def done(self, form_list, **kwargs):  # noqa: D102, C901
        user_registration_form = form_list[3]
        organization = form_list[0].save()
        organization.state_init()  # Set state to "new" and create a state_log entry
        user = handle_user_creation(user_registration_form, organization, self.request)

        # Add new organization instance to inlineformsets for contacts and services, and save them
        for formset in form_list[1:3]:
            formset.instance = organization
            formset.save()

        messages.success(
            self.request,
            _(
                """Organization '%(organization_name)s' has been submitted.
                It still needs to be approved by us.
                We usually review new entries within a week."""
            )
            % {"organization_name": organization.name},
        )

        handle_email_dispatch(organization, user, user_registration_form, self.request)

        # Cleanup: Reset the instance_dict and wizard storage
        self.instance_dict = None
        self.storage.reset()

        # If user is logged in: directly redirect them to the organization preview.
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(f"/{organization.slug}")
        else:
            return HttpResponseRedirect("/")

    def get_context_data(self, form, **kwargs):  # noqa: D102
        context = super().get_context_data(form=form, **kwargs)
        try:
            form_intro = ParlerFlatPage.objects.get(url="/form-intro/")
        except ObjectDoesNotExist:
            form_intro = None
        context["form_intro"] = form_intro
        return context


class Popup(View):
    """Popup view for Services on the map."""

    template_name = "filter/popup.html"

    def get(self, request, *args, **kwargs):  # noqa: D102
        service_ids = request.GET.get("service_ids", "")
        location_id = request.GET.get("location_id", "")
        service_ids = service_ids.split(",")
        try:
            services = Service.objects.filter(id__in=service_ids)
        except ObjectDoesNotExist:
            raise Http404(_("Service does not exist"))

        # Get list of organizations of services
        organizations = Organization.objects.filter(services__in=services).distinct()
        location = PhysicalLocation.objects.get(id=location_id)

        return render(
            request,
            self.template_name,
            {
                "organizations": organizations,
                "services": services,
                "location": location,
            },
        )
