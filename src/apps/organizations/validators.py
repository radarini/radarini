# noqa: D100
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from django.template.defaultfilters import filesizeformat


@deconstructible
class ImageSizeValidator(object):
    """Validator for uploaded image size."""

    error_messages = {
        "max_size": (
            "Ensure this file size is not greater than %(max_size)s."
            " Your file size is %(size)s."
        ),
    }

    def __init__(self, max_size=None):
        self.max_size = max_size

    def __call__(self, data):  # noqa: D102
        if self.max_size is not None and data.size > self.max_size:
            params = {
                "max_size": filesizeformat(self.max_size),
                "size": filesizeformat(data.size),
            }
            raise ValidationError(self.error_messages["max_size"], "max_size", params)

    def __eq__(self, other):
        return isinstance(other, ImageSizeValidator) and self.max_size == other.max_size
