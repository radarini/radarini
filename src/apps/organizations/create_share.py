# noqa: D100
from django.utils.crypto import get_random_string
from apps.organizations.models import ShareKey

from django.utils import timezone


def create_share_key(url, expiration_seconds):  # noqa: D103
    key = ShareKey.objects.create(
        pk=get_random_string(40),
        expiration_date=timezone.now() + timezone.timedelta(seconds=expiration_seconds),
        location=url,
    )
    key.save()
    return key
