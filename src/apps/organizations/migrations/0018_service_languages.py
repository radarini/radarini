# Generated by Django 4.0.3 on 2022-08-22 10:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("filtercategories", "0005_languagecategory_languagecategorytranslation"),
        ("organizations", "0017_servicetranslation"),
    ]

    operations = [
        migrations.AddField(
            model_name="service",
            name="languages",
            field=models.ManyToManyField(
                blank=True,
                related_name="services",
                to="filtercategories.languagecategory",
            ),
        ),
    ]
