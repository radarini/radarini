from django.db import migrations


def update_physicallocation_relation(apps, schema_editor):  # noqa
    Service = apps.get_model("organizations", "Service")
    PhysicalLocation = apps.get_model("locations", "PhysicalLocation")

    for service in Service.objects.all():
        for location in service.locations.all():
            physicallocation_found = False
            for physicallocation in PhysicalLocation.objects.all():
                if physicallocation.location_reference == location:
                    physicallocation_found = True
                    print(f"Adding {physicallocation} for {location}")
                    service.physicallocations.add(physicallocation)
                    break
            if not physicallocation_found:
                raise RuntimeError(f"No matching physicallocation found for {location}")


class Migration(migrations.Migration):  # noqa
    dependencies = [
        ("organizations", "0038_service_physicallocations"),
    ]

    operations = [
        migrations.RunPython(update_physicallocation_relation),
    ]
