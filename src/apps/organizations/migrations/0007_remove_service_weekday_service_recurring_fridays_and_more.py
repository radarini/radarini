# Generated by Django 4.0.3 on 2022-04-01 09:41

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0006_locationcontact_accessible_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="service",
            name="weekday",
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_fridays",
            field=models.BooleanField(default=False, verbose_name="Recurrs on Fridays"),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_mondays",
            field=models.BooleanField(default=False, verbose_name="Recurrs on Mondays"),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_saturdays",
            field=models.BooleanField(
                default=False, verbose_name="Recurrs on Saturdays"
            ),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_sundays",
            field=models.BooleanField(default=False, verbose_name="Recurrs on Sundays"),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_thursdays",
            field=models.BooleanField(
                default=False, verbose_name="Recurrs on Thursdays"
            ),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_tuesdays",
            field=models.BooleanField(
                default=False, verbose_name="Recurrs on Tuesdays"
            ),
        ),
        migrations.AddField(
            model_name="service",
            name="recurring_wednesdays",
            field=models.BooleanField(
                default=False, verbose_name="Recurrs on Wednesdays"
            ),
        ),
    ]
