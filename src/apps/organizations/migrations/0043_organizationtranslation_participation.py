# Generated by Django 5.0.6 on 2024-06-02 01:56

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0042_alter_service_languages"),
    ]

    operations = [
        migrations.AddField(
            model_name="organizationtranslation",
            name="participation",
            field=models.TextField(
                blank=True,
                help_text="Description how one could participated.What are the conditions? What is needed right now?",
                verbose_name="participation",
            ),
        ),
    ]
