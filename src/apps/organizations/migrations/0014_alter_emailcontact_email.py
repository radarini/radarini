# Generated by Django 4.0.3 on 2022-06-10 13:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0013_alter_emailcontact_email_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="emailcontact",
            name="email",
            field=models.EmailField(max_length=250, verbose_name="e-mail address"),
        ),
    ]
