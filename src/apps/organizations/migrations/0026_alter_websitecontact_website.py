# Generated by Django 4.1.2 on 2022-10-24 22:28

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "organizations",
            "0025_alter_organization_logo_alter_organization_slug_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="websitecontact",
            name="website",
            field=models.CharField(
                default="https://",
                max_length=250,
                validators=[
                    django.core.validators.URLValidator(
                        message="Could not read your input as valid URL address. Did you include https://?",
                        schemes=["http", "https"],
                    )
                ],
                verbose_name="website",
            ),
        ),
    ]
