# Generated by Django 4.2.4 on 2023-08-03 18:58

import apps.organizations.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0032_alter_organizationtranslation_alternate_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="organization",
            name="logo",
            field=models.ImageField(
                blank=True,
                help_text="Image that will be displayed for the organization. (Allowed formats: png, jpeg, jpg)",
                null=True,
                upload_to="organizations/%Y/%m/%d/",
                validators=[apps.organizations.validators.ImageSizeValidator(3145728)],
                verbose_name="logo",
            ),
        ),
    ]
