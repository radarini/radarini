# Generated by Django 4.0.3 on 2022-06-07 14:25

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0012_organization_assignees_organization_creator"),
    ]

    operations = [
        migrations.AlterField(
            model_name="emailcontact",
            name="email",
            field=models.CharField(max_length=250, verbose_name="e-mail address"),
        ),
        migrations.AlterField(
            model_name="phonecontact",
            name="telephone",
            field=models.CharField(max_length=50, verbose_name="telephone number"),
        ),
        migrations.AlterField(
            model_name="platformcontact",
            name="platform",
            field=models.CharField(
                choices=[
                    ("FB", "Facebook"),
                    ("TW", "Twitter"),
                    ("IG", "Instagram"),
                    ("MS", "Mastodon"),
                ],
                max_length=2,
                verbose_name="platform",
            ),
        ),
        migrations.AlterField(
            model_name="platformcontact",
            name="profile_name",
            field=models.CharField(max_length=250, verbose_name="profile name"),
        ),
        migrations.AlterField(
            model_name="websitecontact",
            name="website",
            field=models.CharField(max_length=250, verbose_name="website"),
        ),
    ]
