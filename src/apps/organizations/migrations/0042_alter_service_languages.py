# Generated by Django 5.0.3 on 2024-03-15 15:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("filtercategories", "0005_languagecategory_languagecategorytranslation"),
        ("organizations", "0041_service_recurring_weekly"),
    ]

    operations = [
        migrations.AlterField(
            model_name="service",
            name="languages",
            field=models.ManyToManyField(
                blank=True,
                help_text="Select the offered language(s) of the service. If a language is missing <a href='/contact' class='underline'>contact us</a>.",
                related_name="services",
                to="filtercategories.languagecategory",
            ),
        ),
    ]
