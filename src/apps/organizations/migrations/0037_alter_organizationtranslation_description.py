# Generated by Django 4.2.4 on 2023-08-24 00:24

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0036_set_default_lang"),
    ]

    operations = [
        migrations.AlterField(
            model_name="organizationtranslation",
            name="description",
            field=models.TextField(
                help_text="The general description of the organization. What they do and with what motivation and specifics. These contents also appear in the free text search. Please do not enter detailed time information. The information might get outdated. The visitors should always need to check your website. (~500 characters)",
                verbose_name="description",
            ),
        ),
    ]
