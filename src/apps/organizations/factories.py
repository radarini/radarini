# noqa: D100
import factory
import factory.fuzzy
import random

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from apps.filtercategories.factories import (
    TopicCategoryFactory,
    ServiceCategoryFactory,
    FeatureCategoryFactory,
    LanguageCategoryFactory,
    ClassificationCategoryFactory,
)
from apps.filtercategories.models import (
    TopicCategory,
    ServiceCategory,
    FeatureCategory,
    LanguageCategory,
    ClassificationCategory,
)
from apps.organizations.models import (
    Organization,
    Service,
    PhoneContact,
    WebsiteContact,
    EmailContact,
    PlatformContact,
)
from apps.locations.factories import PhysicalLocationFactory
from apps.locations.models import PhysicalLocation
from typing import List


def get_or_create_bulk(
    model: models.Model,
    model_factory: factory.django.DjangoModelFactory,
    num_instances: int = 2,
) -> List[models.Model]:
    """Return a random sample of model instances.

    The instance type is determined by `model` parameter.
    The number of returned instances can be controlled via `num_instances`.
    If not enough instances exist, they will be created using the `model_factory`.
    """
    num_existing = model.objects.count()
    if num_existing < num_instances:
        for _ in range(num_instances - num_existing):
            model_factory()
    return random.sample(list(model.objects.all()), num_instances)


class OrganizationFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake organization entities."""

    class Meta:  # noqa: D106
        model = Organization

    name = factory.Faker("company", locale=settings.FACTORY_LOCALE)
    description = factory.Faker("catch_phrase", locale=settings.FACTORY_LOCALE)
    slug = factory.Faker("slug", locale=settings.FACTORY_LOCALE)

    @factory.post_generation
    def topics(self, create: bool, categories: List[TopicCategory], **kwargs):
        """Relates a generated `Organisation` to many `TopicCategory` entities.

        Categories can be added via the Factory constructor.
        Example: `OrganizationFactory(topics=[t1, t2, t3])`

        If no categories are specified,
        they will be sampled from existing `TopicCategory` items.
        If not enough entities exist they will be generated.

        The default number of associated categories is 2.
        To specify the number, use the `topics__num` argument.
        Example: `OrganizationFactory(topics__num=3)`
        """
        if not create:
            return

        DEFAULT_NUM_TOPICS = 2
        if not categories:
            # topics__ prefix is stripped in kwargs
            num_topics = kwargs.get("num", DEFAULT_NUM_TOPICS)
            self.topics.add(
                *get_or_create_bulk(TopicCategory, TopicCategoryFactory, num_topics)
            )
        else:
            self.topics.add(*categories)


class ServiceFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake organization entities."""

    class Meta:  # noqa: D106
        model = Service

    organization = factory.SubFactory(OrganizationFactory)

    @factory.lazy_attribute
    def service_category(self):
        """Relates a service to a service category.

        If no or only one `ServiceCategory` exists,
        an additional one will be generated using `ServiceCategoryFactory`.
        If more than one service category already exists,
        a random choice will be made.
        """
        if ServiceCategory.objects.count() <= 1:
            return ServiceCategoryFactory()
        else:
            return random.choice(list(ServiceCategory.objects.all()))

    @factory.post_generation
    def features(self, create: bool, categories: List[FeatureCategory], **kwargs):
        """Relates a generated `Service` to many `FeatureCategory` entities.

        Categories can be added via the Factory constructor.
        Example: `ServiceFactory(features=[f1, f2, f3])`

        If no categories are specified,
        they will be sampled from existing `FeatureCategory` items.
        If not enough categories exist they will be generated.

        The default number of associated categories is 2.
        To specify the number, use the `features__num` argument.
        Example: `ServiceFactory(features__num=3)`
        """
        if not create:
            return

        DEFAULT_NUM_FEATURES = 2
        if not categories:
            # features__ prefix is stripped in kwargs
            num_features = kwargs.get("num", DEFAULT_NUM_FEATURES)
            self.features.add(
                *get_or_create_bulk(
                    FeatureCategory, FeatureCategoryFactory, num_features
                )
            )
        else:
            self.features.add(*categories)

    @factory.post_generation
    def languages(self, create: bool, categories: List[LanguageCategory], **kwargs):
        """Relates a generated `Service` to many `LanguageCategory` entities.

        Categories can be added via the Factory constructor.
        Example: `ServiceFactory(lanuages=[l1, l2, l3])`

        If no categories are specified,
        they will be sampled from existing `LanguageCategory` items.
        If not enough categories exist they will be generated.

        The default number of associated categories is 2.
        To specify the number, use the `languages__num` argument.
        Example: `ServiceFactory(languages__num=3)`
        """
        if not create:
            return

        DEFAULT_NUM_LANGUAGES = 2
        if not categories:
            # features__ prefix is stripped in kwargs
            num_languages = kwargs.get("num", DEFAULT_NUM_LANGUAGES)
            self.languages.add(
                *get_or_create_bulk(
                    LanguageCategory, LanguageCategoryFactory, num_languages
                )
            )
        else:
            self.languages.add(*categories)

    @factory.post_generation
    def classifications(
        self, create: bool, categories: List[ClassificationCategory], **kwargs
    ):
        """Relates a generated `Service` to many `ClassificationCategory` entities.

        Categories can be added via the Factory constructor.
        Example: `ServiceFactory(classifications=[c1, c2, c3])`

        If no categories are specified,
        they will be sampled from existing `ClassificationCategory` items.
        If not enough categories exist they will be generated.

        The default number of associated categories is 2.
        To specify the number, use the `classification__num` argument.
        Example: `ServiceFactory(classification__num=3)`
        """
        if not create:
            return

        DEFAULT_NUM_CLASSIFICATIONS = 2
        if not categories:
            # classifications__ prefix is stripped in kwargs
            num_classification = kwargs.get("num", DEFAULT_NUM_CLASSIFICATIONS)
            self.classifications.add(
                *get_or_create_bulk(
                    ClassificationCategory,
                    ClassificationCategoryFactory,
                    num_classification,
                )
            )
        else:
            self.classifications.add(*categories)

    @factory.post_generation
    def physicallocations(
        self, create: bool, physicallocations: List[PhysicalLocation], **kwargs
    ):
        """Relates a generated `Service` to one or many `PhysicalLocation` entities.

        Categories can be added via the Factory constructor.
        Example: `ServiceFactory(physicallocations=[l1, l2, l3])`

        If no physicallocations are specified, a new one is generated.
        """
        if not create:
            return

        if not physicallocations:
            self.physicallocations.add(PhysicalLocationFactory())
        else:
            self.physicallocations.add(*physicallocations)

    recurring_mondays = factory.Faker("pybool")
    recurring_tuesdays = factory.Faker("pybool")
    recurring_wednesdays = factory.Faker("pybool")
    recurring_thursdays = factory.Faker("pybool")
    recurring_fridays = factory.Faker("pybool")
    recurring_saturdays = factory.Faker("pybool")
    recurring_sundays = factory.Faker("pybool")


class ContactFactory(factory.django.DjangoModelFactory):
    """Abstract base factory for contacts.

    In the implementing factory,
    make sure to pass a related object via the `content_type` argument.
    """

    object_id = factory.SelfAttribute("content_object.id")
    content_type = factory.LazyAttribute(
        lambda o: ContentType.objects.get_for_model(o.content_object)
    )

    class Meta:  # noqa: D106
        exclude = ["content_object"]
        abstract = True


class PhoneContactFactory(ContactFactory):
    """Factory to generate fake phone contact entities."""

    class Meta:  # noqa: D106
        model = PhoneContact

    # TODO this is not verififed, dont know how to enforce a certain format
    telephone = factory.Faker("phone_number", locale=settings.FACTORY_LOCALE)
    service = factory.fuzzy.FuzzyChoice(
        PhoneContact.Service.choices, getter=lambda c: c[0]
    )


class WebsiteContactFactory(ContactFactory):
    """Factory to generate fake website contact entities."""

    class Meta:  # noqa: D106
        model = WebsiteContact

    website = factory.Faker("uri")


class EmailContactFactory(ContactFactory):
    """Factory to generate fake email contact entities."""

    class Meta:  # noqa: D106
        model = EmailContact

    email = factory.Faker("ascii_email")


class PlatformContactFactory(ContactFactory):
    """Factory to generate fake platform contact entities."""

    class Meta:  # noqa: D106
        model = PlatformContact

    platform = factory.fuzzy.FuzzyChoice(
        PlatformContact.Platform.choices, getter=lambda c: c[0]
    )
    profile_name = factory.Faker("user_name")
