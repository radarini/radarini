from django.contrib.auth import login  # noqa: D100
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext as _
from django.conf import settings

from apps.organizations.send_mail_async import send_mail_async
from apps.organizations.models import EmailContact
from apps.organizations.create_share import create_share_key

from urllib.parse import quote


def handle_user_creation(form, organization, request):  # noqa: D102, D100
    """Handle user creation and login."""
    user = None

    if request.user.is_authenticated:
        user = request.user

    elif (
        form.cleaned_data["email"]
        and form.cleaned_data["username"]
        and form.cleaned_data["password1"]
    ):
        user = form.save()
        login(request, user)

    if user:
        organization.creator = user

        # Don't assign admins
        if not user.is_superuser:
            organization.assignees.add(user)

        if "3-member" in form.data:
            organization.members.add(user)
            organization.verified = True

        organization.save()

    return user


def send_mail_admin_notify(
    organization, request, organization_email_contacts, site, title=None
):  # noqa: D102, D100
    """Sent mail with pre-formulated text to admins to notify organization about the submission."""
    if not title:
        title = (
            f"[{settings.HOST}] {organization.name} "
            "wurde eingetragen und noch nicht benachrichtigt"
        )
    verify_key = create_share_key(
        url=organization.get_absolute_url(),
        expiration_seconds=60 * 60 * 24 * 7 * 4,  # 4 Weeks
    )
    signup_key = create_share_key(
        url=reverse("accounts:signup", kwargs={"slug": organization.slug}),
        expiration_seconds=60 * 60 * 24 * 7 * 4,  # 4 Weeks
    )
    recipients = [settings.SUBMISSION_REPORT_MAIL]
    msg_orga = generate_orga_mail(organization, request, signup_key, verify_key)
    mail_title_orga = f"[{settings.HOST}] {organization.name} wurde auf {site.settings.title} eingetragen"
    msg_admin = render_to_string(
        "organizations/partials/orga_creation_email_admins.html",
        {
            "organization": organization,
            "signup_key": signup_key,
            "verify_key": verify_key,
            "encoded_orga_msg": quote(msg_orga),
            "organization_email_contacts": organization_email_contacts,
            "mail_title_orga": mail_title_orga,
            "host_name": settings.HOST,
        },
        request=request,
    )

    # TODO: Create plain text version of msg_admin

    send_mail_async(
        subject=title,
        text_content=msg_admin,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipients,
        html_content=msg_admin,
    )


def generate_orga_mail(organization, request, signup_key, verify_key):  # noqa: D102, D100, D103
    """Generate the mail for the organization."""
    msg_orga = render_to_string(
        "organizations/partials/orga_creation_email_orga.html",
        {
            "organization": organization,
            "signup_key": signup_key,
            "verify_key": verify_key,
            "support_mail": settings.SUPPORT_MAIL,
            "host_name": settings.HOST,
        },
        request=request,
    )
    return msg_orga


def send_mail_orga_submit(organization, request, site, organization_email_contacts):  # noqa: D102, D100
    """Inform about the submission, ask for verification and offer an assigned account."""
    mail_title_orga = f"[{settings.HOST}] {organization.name} wurde auf {site.settings.title} eingetragen"

    verify_key = create_share_key(
        url=organization.get_absolute_url(),
        expiration_seconds=60 * 60 * 24 * 7 * 2,  # 4 Weeks
    )
    signup_key = create_share_key(
        url=reverse("accounts:signup", kwargs={"slug": organization.slug}),
        expiration_seconds=60 * 60 * 24 * 7 * 2,  # 4 Weeks
    )

    msg_orga = generate_orga_mail(organization, request, signup_key, verify_key)

    recipients = [
        settings.SUBMISSION_REPORT_MAIL,
        *organization_email_contacts,
    ]

    send_mail_async(
        subject=mail_title_orga,
        text_content=msg_orga,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipients,
    )


def send_mail_user_success(organization, user, request, site):  # noqa: D102, D100
    """Inform user about successfull submission."""
    title = _(
        "[%(host_name)s] You submitted %(organization_name)s to %(site_name)s"
    ) % {
        "organization_name": organization.name,
        "host_name": settings.HOST,
        "site_name": site.settings.title,
    }
    recipients = [user.email]

    msg = render_to_string(
        "organizations/partials/orga_creation_email_creator.html",
        {
            "organization": organization,
            "support_mail": settings.SUPPORT_MAIL,
            "host_name": settings.HOST,
        },
        request=request,
    )

    send_mail_async(
        subject=title,
        text_content=msg,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipients,
    )


def handle_email_dispatch(organization, user, user_registration_form, request):  # noqa: D102, D100
    """Send emails to organization email contacts and user."""
    site = Site.objects.get_current()
    organization_email_contacts = [
        e.email for e in organization.contacts.instance_of(EmailContact).all()
    ]

    # A normal user was logged in or created during the orga submission
    if user and not user.is_superuser:
        if user.email in organization_email_contacts:
            organization.verified = True

        send_mail_admin_notify(
            organization=organization,
            request=request,
            organization_email_contacts=organization_email_contacts,
            site=site,
        )
        send_mail_user_success(
            organization=organization, user=user, request=request, site=site
        )

    # The orga was submitted by an admin
    elif user and user.is_superuser:
        # Only send mail to organization email contacts if the admin selected the checkbox
        if organization_email_contacts and (
            user_registration_form.data.get("3-send-email") == "on"
        ):
            send_mail_orga_submit(
                organization=organization,
                request=request,
                organization_email_contacts=organization_email_contacts,
                site=site,
            )
        else:
            send_mail_admin_notify(
                organization=organization,
                request=request,
                organization_email_contacts=organization_email_contacts,
                site=site,
            )

    # Anonymous submission: no user was created or logged in during submission.
    else:
        title = f"[{settings.HOST}] {organization.name} wurde anonym eingetragen und noch nicht benachrichtigt"
        send_mail_admin_notify(
            organization=organization,
            request=request,
            organization_email_contacts=organization_email_contacts,
            site=site,
            title=title,
        )
