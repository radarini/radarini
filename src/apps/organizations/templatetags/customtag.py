# noqa: D100
from django import template

register = template.Library()


@register.simple_tag
def replace_url_value(request, **kwargs):  # noqa: D103
    updated = request.GET.copy()
    for k, v in kwargs.items():
        if v is not None:
            updated[k] = v
        else:
            updated.pop(k, 0)

    return updated.urlencode()


@register.filter("input_type")
def input_type(input):  # noqa: D103
    return input.field.widget.__class__.__name__


@register.filter("multiply")
def multiply(value, arg):  # noqa: D103
    return str(str(value) * int(arg))


@register.filter(name="remove")
def remove(value, arg):  # noqa: D103
    return value.replace(arg, "")
