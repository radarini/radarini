# noqa: D100
from django import template

register = template.Library()


@register.simple_tag
def get_name(queryset, id):  # noqa: D103
    return queryset.get(id=id).name
