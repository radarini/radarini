# noqa: D100
from django import template

register = template.Library()


@register.filter
def email_obfuscation(email):  # noqa: D103
    return email.replace("@", "[at]").replace(".", "[dot]").replace("-", "[dash]")
