from django.urls import path, register_converter
from . import views, converters

register_converter(converters.LowerCaseConverter, "lowerc_slug")

app_name = "organizations"

urlpatterns = [
    path("", views.List.as_view(), name="index"),
    path("list/", views.List.as_view(), name="list"),
    path("map/", views.Map.as_view(), name="map"),
    path(
        "submit/",
        views.OrganzationSubmitWizardView.as_view(),
        name="submit-organization",
    ),
    path(
        "dashboard/",
        views.AdminDashboard.as_view(),
        name="dashboard",
    ),
    path(
        "promote/",
        views.PromotedOrganizations.as_view(),
        name="promote",
    ),
    path("popup/", views.Popup.as_view(), name="popup"),
    path(
        "<lowerc_slug:slug>/",
        views.OrganizationDetail.as_view(),
        name="show",
    ),
    path(
        "<lowerc_slug:slug>/edit/",
        views.OrganizationUpdate.as_view(),
        name="edit-organiziation",
    ),
    path(
        "<lowerc_slug:slug>/edit/services/",
        views.ServiceUpdate.as_view(),
        name="edit-services",
    ),
    path(
        "<lowerc_slug:slug>/edit/contacts/",
        views.ContactUpdate.as_view(),
        name="edit-contacts",
    ),
    path("access/<str:key>/", views.SharedPage.as_view(), name="sharedPage"),
]
