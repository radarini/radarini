from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import gettext_lazy as _

from parler.admin import TranslatableAdmin
from parler.forms import TranslatableModelForm

from .models import ParlerFlatPage


class ParlerFlatPageForm(TranslatableModelForm, FlatpageForm):  # noqa: D101
    class Meta:  # noqa: D106
        model = ParlerFlatPage
        fields = "__all__"


class ParlerFlatPageAdmin(
    TranslatableAdmin,
    FlatPageAdmin,
):
    """An admin model that merges parler and flatpages.

    Instead of title and content the translatable fields title_translated and content_translated are shown.
    """

    list_display = ("url", "title_translated")
    form = ParlerFlatPageForm
    fieldsets = (
        (None, {"fields": ("url", "title_translated", "content_translated", "sites")}),
        (
            _("Advanced options"),
            {
                "classes": ("collapse",),
                "fields": ("registration_required", "template_name"),
            },
        ),
    )


# Unregister the base FlatPage model
admin.site.unregister(FlatPage)
# Register our ParlerFlatPage with our custom admin model
admin.site.register(ParlerFlatPage, ParlerFlatPageAdmin)
