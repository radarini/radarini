# noqa: D100
import factory
from django.conf import settings
from apps.flatpages_parler.models import ParlerFlatPage


class ParlerFlatPageFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake organization entities."""

    class Meta:  # noqa: D106
        model = ParlerFlatPage

    title_translated = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)
    content_translated = factory.Faker(
        "paragraph", nb_sentences=5, locale=settings.FACTORY_LOCALE
    )
