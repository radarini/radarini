from django.db import models
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import gettext_lazy as _

from parler.models import TranslatableModel, TranslatedFields
from parler.managers import TranslationManager


class ParlerFlatPage(FlatPage, TranslatableModel):
    """A FlatPage model with new title_translated and content_translated fields.

    Field content is translatable via django-parler.
    """

    translations = TranslatedFields(
        title_translated=models.CharField(_("title"), max_length=200),
        content_translated=models.TextField(_("content"), blank=True),
    )
    # We need to set the default manager to TranslationManager, so that everything works.
    objects = TranslationManager()
