from django.apps import AppConfig


class FlatpagesParlerConfig(AppConfig):  # noqa: D101
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.flatpages_parler"
