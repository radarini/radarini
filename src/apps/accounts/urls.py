from django.urls import path, reverse_lazy
from .views import (
    SignUpView,
    CustomLoginView,
    ProfileUpdateView,
    CustomPasswordChangeView,
    CustomPasswordResetView,
    AccountDeleteView,
    ProfileOrganizationsView,
)
from django.contrib.auth import views as auth_views

app_name = "accounts"

urlpatterns = [
    path("signup/", SignUpView.as_view(), name="signup"),
    path("signup/<slug>/", SignUpView.as_view(), name="signup"),
    path("login/", CustomLoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path(
        "profile/",
        ProfileUpdateView.as_view(),
        name="profile",
    ),
    path(
        "organizations/",
        ProfileOrganizationsView.as_view(),
        name="profile_organizations",
    ),
    path(
        "password_change/",
        CustomPasswordChangeView.as_view(template_name="accounts/password_change.html"),
        name="password_change",
    ),
    path("password_reset/", CustomPasswordResetView.as_view(), name="password_reset"),
    path(
        "password_reset/done/",
        auth_views.PasswordResetView.as_view(
            template_name="accounts/password_reset_done.html"
        ),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="accounts/password_reset_confirm.html",
            success_url=reverse_lazy("accounts:password_reset_complete"),
        ),
        name="password_reset_confirm",
    ),
    path(
        "password_reset_complete/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="accounts/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path(
        "delete",
        AccountDeleteView.as_view(template_name="accounts/delete.html"),
        name="delete",
    ),
]
