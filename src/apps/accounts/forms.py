from django import forms
from django.contrib.auth.forms import (
    UserCreationForm,
    AuthenticationForm,
)
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.utils.decorators import method_decorator
from honeypot.decorators import check_honeypot


@method_decorator(check_honeypot, name="check_honeypot")
class SignUpForm(UserCreationForm):  # noqa: D101
    email = forms.EmailField(required=True)
    usable_password = None

    class Meta:  # noqa: D106
        model = User
        fields = ["username", "email", "password1", "password2"]

    def check_honeypot(self, request):  # noqa: D102
        return True

    def clean_email(self):  # noqa: D102
        email = User.objects.normalize_email(self.cleaned_data["email"]).lower()
        if email and User.objects.filter(email=email).exists():
            raise ValidationError(_("A user with this email already exists."))
        return email


class OptionalSignUpForm(SignUpForm):
    """Used in the submit wizard to optionally create a user account."""

    template_name = "accounts/forms/optional_signup_form.html"

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        self.label_suffix = ""  # Removes : as label suffix
        self.fields["username"].required = False
        self.fields["username"].help_text = _(
            "150 characters or fewer. Letters, digits and @/./+/-/_ only."
        )
        self.fields["email"].required = False
        self.fields["password1"].required = False
        self.fields["password2"].required = False

    class Meta(SignUpForm.Meta):  # noqa: D106
        include = ["member"]

    member = forms.BooleanField(
        label=_("I am a member of the submitted organization."),
        required=False,
        widget=forms.CheckboxInput(attrs={"class": "opt-submit-checkbox"}),
    )

    def clean(self):
        """Validate input.

        Because all fields are optional if ANY field was filled in
        we must throw an ValidationError if not ALL other fields have been filled in.
        """
        cleaned_data_values = [
            self.cleaned_data.get("username"),
            self.cleaned_data.get("email"),
            self.cleaned_data.get("password1"),
            self.cleaned_data.get("password2"),
            # Don't check member field
        ]

        if any(cleaned_data_values) and not all(cleaned_data_values):
            raise ValidationError(_("Some fields are missing or have errors."))


class LoginForm(AuthenticationForm):  # noqa: D101
    username = forms.CharField(max_length=150, required=True, widget=forms.TextInput())
    password = forms.CharField(
        max_length=150, required=True, widget=forms.PasswordInput()
    )

    class Meta:  # noqa: D106
        model = User
        fields = ["username", "password"]


class ProfileUpdateForm(forms.ModelForm):  # noqa: D101
    username = forms.CharField(max_length=150, required=True, widget=forms.TextInput())
    email = forms.EmailField(max_length=150, required=True, widget=forms.TextInput())

    class Meta:  # noqa: D106
        model = User
        fields = ["username", "email"]

    def clean_email(self):  # noqa: D102
        value = User.objects.normalize_email(self.cleaned_data["email"]).lower()
        try:
            existing_user = User.objects.get(email=value)
        except User.DoesNotExist:
            return value
        if existing_user.pk != self.instance.pk:
            raise ValidationError(_("A user with this email already exists."))
        return value

    def clean_username(self):  # noqa: D102
        return User.normalize_username(self.cleaned_data["username"]).lower()
