from django.shortcuts import redirect
from .forms import SignUpForm, ProfileUpdateForm
from django.contrib.auth import login
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordResetView
from django.views.generic.edit import FormView, DeleteView, UpdateView
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import View
from django.shortcuts import render, get_object_or_404

from apps.organizations.models import Organization


class SignUpView(FormView):  # noqa: D101
    form_class = SignUpForm
    initial = {"key": "value"}
    template_name = "accounts/signup.html"
    success_url = "organizations:index"

    def form_valid(self, form):  # noqa: D102
        user = form.save()
        login(self.request, user)
        messages.success(self.request, _("Your account has been successfully created."))
        slug = self.kwargs.get("slug", None)
        if self.kwargs.get("shared", None) and slug:
            organization = get_object_or_404(Organization, slug__iexact=slug)
            organization.assignees.add(user)
            organization.save()
            return redirect(organization)

        return redirect("organizations:index")

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if request.user.is_authenticated:
            return redirect(to="/")
        return super(SignUpView, self).dispatch(request, *args, **kwargs)


class CustomLoginView(LoginView):  # noqa: D101
    initial = {"key": "value"}
    template_name = "accounts/login.html"
    success_url = "organizations:index"

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if request.user.is_authenticated:
            return redirect(to="/")
        return super(CustomLoginView, self).dispatch(request, *args, **kwargs)


class ProfileUpdateView(SuccessMessageMixin, UpdateView):  # noqa: D101
    model = User
    form_class = ProfileUpdateForm
    template_name = "accounts/profile.html"
    success_url = reverse_lazy("accounts:profile")
    success_message = _("Your profile data has been updated.")

    def get_success_message(self, cleaned_data):  # noqa: D102
        return self.success_message % cleaned_data

    def get_object(self):  # noqa: D102
        return self.request.user

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not request.user.is_authenticated:
            return redirect(to="accounts:login")
        return super(ProfileUpdateView, self).dispatch(request, *args, **kwargs)


class ProfileOrganizationsView(View):  # noqa: D101
    model = User
    template_name = "accounts/organizations.html"

    def get(self, request):  # noqa: D102
        if not request.user.is_authenticated:
            return redirect(to="accounts:login")

        organizations = Organization.objects.filter(
            assignees__username__iexact=request.user.username
        )

        return render(
            request,
            self.template_name,
            {"organizations": organizations},
        )


class CustomPasswordChangeView(SuccessMessageMixin, PasswordChangeView):  # noqa: D101
    template_name = "accounts/password_change.html"
    success_url = reverse_lazy("accounts:password_reset")
    success_message = _("Your password has been changed.")

    def get_success_message(self, cleaned_data):  # noqa: D102
        return self.success_message % cleaned_data

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not request.user.is_authenticated:
            return redirect(to="accounts:login")
        return super(CustomPasswordChangeView, self).dispatch(request, *args, **kwargs)


class CustomPasswordResetView(PasswordResetView, SuccessMessageMixin):  # noqa: D101
    template_name = "accounts/password_reset.html"
    email_template_name = "accounts/password_reset_email.html"
    success_url = reverse_lazy("accounts:password_reset_done")

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if request.user.is_authenticated:
            return redirect(to="accounts:password_change")
        return super(CustomPasswordResetView, self).dispatch(request, *args, **kwargs)


class AccountDeleteView(SuccessMessageMixin, DeleteView):  # noqa: D101
    model = User
    template_name = "accounts/delete.html"
    success_url = reverse_lazy("accounts:login")
    success_message = _("Your account has been deleted.")

    def get_success_message(self, cleaned_data):  # noqa: D102
        return self.success_message % cleaned_data

    def get_object(self):  # noqa: D102
        return self.request.user

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not request.user.is_authenticated:
            return redirect(to="accounts:login")
        return super(AccountDeleteView, self).dispatch(request, *args, **kwargs)
