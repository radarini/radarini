"""Module to import tanslations from po files."""

from pathlib import Path
from typing import Optional
from django.core.management.base import BaseCommand
from apps.flatpages_parler.models import ParlerFlatPage
from parler.models import TranslatableModel
from apps.filtercategories.models import (
    TopicCategory,
    FeatureCategory,
    ClassificationCategory,
    LanguageCategory,
    ServiceCategory,
)
from apps.site_settings.models import SettingsTranslation
from config.settings.base import PARLER_LANGUAGES

import polib
import gettext


class Command(BaseCommand):
    """Management command to import translations from po files into the database."""

    source_lang: Optional[str] = None
    target_lang: Optional[str] = None
    import_dir: Optional[str] = None

    default_language = PARLER_LANGUAGES["default"]["fallbacks"][0]

    help = (
        "Import translations from po file into the database. - "
        "This command reads the .po files that could be created by the exporttranslations command. "
        "It updates the fields in the models of the given language. "
        "The command expects the files to be located in the data/translations subdirectories. "
        "This command will fail, if the source language has no po file."
        "This command will override the content of the database. "
        "So make sure that there is no translation missing in the po file, that is present in the database. "
        "If the string in the source language changed, the import for that object will fail. "
        "There are hardcoded excludes for the 'privacy-policy' page."
    )

    import_tasks = [
        {
            "name": "TopicCategory",
            "model": TopicCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "FeatureCategory",
            "model": FeatureCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ClassificationCategory",
            "model": ClassificationCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ServiceCategory",
            "model": ServiceCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ParlerFlatPage",
            "model": ParlerFlatPage,
            "fields": ["content_translated"],
            "excludes": ["/privacy-policy/ -- "],
        },
        {
            "name": "LanguageCategory",
            "model": LanguageCategory,
            "fields": ["name"],
            "excludes": [],
        },
        {
            "name": "SettingsTranslation",
            "model": SettingsTranslation,
            "fields": ["title", "subtitle", "meta_keywords", "meta_description"],
            "excludes": [],
        },
    ]

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "source_lang",
            type=str,
            help=(
                "The ISO 639-1 code of the language that was used as source for the translation"
                "(e.g. 'de' for german). This will be used do determine the in which directory the"
                "translation file is located."
            ),
        )
        parser.add_argument(
            "target_lang",
            type=str,
            help=(
                "The ISO 639-1 code of the language that should be added to the database"
                "(e.g. 'de' for german). Must be part of the activated languages for this instance."
            ),
        )

    def handle(self, *args, **options):  # noqa: D102
        for lang_code in ["source_lang", "target_lang"]:
            if not Command.is_registered_language(options[lang_code]):
                self.stderr.write(
                    self.style.ERROR(
                        f"The language code {options[lang_code]} is not part of the available languages. "
                        f"Please make sure that the language is activated."
                    )
                )
                exit()

        self.source_lang = options["source_lang"]
        self.target_lang = options["target_lang"]

        self.import_dir = f"data/translations/source-{self.source_lang}"
        if not Path(self.import_dir).is_dir():
            raise NoExportFound(self.source_lang)

        base_path = f"{self.import_dir}/{self.target_lang}/LC_MESSAGES/content."
        po = polib.pofile(f"{base_path}po")
        po.save_as_mofile(f"{base_path}mo")

        translations = gettext.translation(
            "content", localedir=self.import_dir, languages=[self.target_lang]
        )
        translations.install()

        self.execute_import_tasks()
        self.stdout.write(self.style.SUCCESS("Imported translation"))

    def execute_import_tasks(self):
        """
        Update the database strings for the objects described in the import_tasks.

        For each task, the objects of the model described are updated in the database
        with the text found in the PO file.
        """
        print(f"Starting import {self.target_lang}")
        for import_task in self.import_tasks:
            objects = import_task["model"].objects.all()
            for obj in objects:
                for field in import_task["fields"]:
                    if obj.__str__() in import_task["excludes"]:
                        continue
                    self.save_translation(obj, field)
            print(f"✓ {import_task['name']}")

    def save_translation(self, obj: TranslatableModel, field: str):
        """
        Save the translation for a given field on a given object.

        This will do nothing if the field is empty in the source language.
        """
        msgid = obj.safe_translation_getter(field, language_code=self.source_lang)
        if msgid == "":
            return
        obj.set_current_language(self.target_lang)
        setattr(obj, field, _(msgid))  # noqa: F821
        obj.save()

    @staticmethod
    def is_registered_language(code: str):
        """Check if language code is activated in parler."""
        return code in [lang["code"] for lang in PARLER_LANGUAGES[1]]


class NoExportFound(Exception):  # noqa: D101
    def __init__(self, lang_code, *args):
        super().__init__(args)
        self.lang_code = lang_code

    def __str__(self):
        return f"There was no export directory found for the language {self.lang_code}"
