"""Module to export the translatable content from the database into po files."""

from django.core.management.base import BaseCommand
from apps.flatpages_parler.models import ParlerFlatPage
from parler.models import TranslatableModel
from django.utils.translation import gettext_lazy as _
from apps.filtercategories.models import (
    TopicCategory,
    FeatureCategory,
    ClassificationCategory,
    LanguageCategory,
    ServiceCategory,
)
from apps.site_settings.models import SettingsTranslation
from config.settings.base import PARLER_LANGUAGES
import polib
import os
import datetime
import contextlib


class Command(BaseCommand):
    """Management command to export the translatable content from the database into po files."""

    help = (
        "Export the translatable content from the database to edit it with an external tool. - "
        "This command creates .po files for the translatable fields in the models. "
        "The files are saved in the newly created content-<source_lang> directory, "
        "which is located in data/translations. This command will fail, "
        "if the source language is missing translations, that are availabe in the default language. "
        "In other words, the export only works on completed source languages. "
        "This limitation was added to make the translation process as clear as possible "
        "and to avoid subsequent corrections. "
        "There are hardcoded excludes for the 'privacy-policy' page."
    )

    export_tasks = [
        {
            "name": "TopicCategory",
            "model": TopicCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "FeatureCategory",
            "model": FeatureCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ClassificationCategory",
            "model": ClassificationCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ServiceCategory",
            "model": ServiceCategory,
            "fields": ["name", "description"],
            "excludes": [],
        },
        {
            "name": "ParlerFlatPage",
            "model": ParlerFlatPage,
            "fields": ["content_translated", "title_translated"],
            "excludes": ["/privacy-policy/ -- "],
        },
        {
            "name": "LanguageCategory",
            "model": LanguageCategory,
            "fields": ["name"],
            "excludes": [],
        },
        {
            "name": "SettingsTranslation",
            "model": SettingsTranslation,
            "fields": ["title", "subtitle", "meta_keywords", "meta_description"],
            "excludes": [],
        },
    ]
    default_language = PARLER_LANGUAGES["default"]["fallbacks"][0]
    bug_mail = os.environ.get("ERROR_REPORT_MAIL", "nomail@example.org")
    current_time = datetime.datetime.now(datetime.timezone.utc).strftime(
        "%Y-%m-%d %H:%M%z"
    )

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "source_lang",
            type=str,
            help=_(
                "The ISO 639-1 code of the language that should be used as the"
                "source for the translation file (e.g. 'de' for german)."
                "Must be part of the activated languages for this instance."
            ),
        )

    def handle(self, *args, **options):  # noqa: D102
        if not self.is_registered_language(options["source_lang"]):
            self.stderr.write(
                self.style.ERROR(
                    f"The language code {options['source_lang']} is not part of the available languages. "
                    f"Please make sure that the language is activated."
                )
            )
            exit()

        self.source_lang = options["source_lang"]

        self.export_dir = f"data/translations/source-{self.source_lang}"
        os.makedirs(self.export_dir, exist_ok=True)

        for lang in PARLER_LANGUAGES[1]:
            target_lang = lang["code"]
            if target_lang == self.source_lang:
                continue
            po_object = self.create_po_object()
            try:
                self.execute_export_tasks(po_object, target_lang)
            except (
                ObjectNotTranslatedInSourceLanguage,
                OptionalFieldTranslationMissing,
            ) as ex:
                self.cleanup_on_error()
                self.stderr.write(self.style.ERROR(ex.__str__()))
                exit()
            language_dir = f"{self.export_dir}/{target_lang}/LC_MESSAGES"
            os.makedirs(language_dir, exist_ok=True)
            file = f"{language_dir}/content.po"
            po_object.save(file)
            print(f"Exported {file}\n")

        self.stdout.write(self.style.SUCCESS("Created translation export"))

    @staticmethod
    def is_registered_language(code: str):
        """Check if language code is activated in parler."""
        return code in [lang["code"] for lang in PARLER_LANGUAGES[1]]

    def execute_export_tasks(
        self,
        po_object: polib.POFile,
        target_lang: str,
    ):
        """
        Create PO entries for the objects described in the export_tasks.

        For each task, the objects of the model described are loaded and the
        appropriate fields are appended to the PO file.
        """
        print(f"Starting export {self.source_lang} -> {target_lang} ...")
        for export_task in self.export_tasks:
            objects = export_task["model"].objects.all()
            for obj in objects:
                for field in export_task["fields"]:
                    if obj.__str__() in export_task["excludes"]:
                        continue
                    entry = self.create_po_entry(obj, field, target_lang)
                    po_object.append(entry)
            print(f"✓ {export_task['name']}")

    def create_po_entry(self, obj: TranslatableModel, field: str, target_lang: str):
        """
        Create po entry from the given object, field and language.

        Throws exception when translations are missing.
        """
        available_languages = obj.get_available_languages()

        if self.source_lang not in available_languages:
            raise ObjectNotTranslatedInSourceLanguage(obj, self.source_lang)

        msgid = obj.safe_translation_getter(field, language_code=self.source_lang)

        msgid_default = obj.safe_translation_getter(
            field, language_code=self.default_language
        )

        # Raise exception when there is an empty field in the source language, that is not empty in the
        # default language. In other words an optional field has not been translated.
        if not msgid and msgid_default:
            raise OptionalFieldTranslationMissing(obj, field)

        msgstr = ""

        if target_lang in available_languages:
            msgstr = obj.safe_translation_getter(field, language_code=target_lang)

        entry = polib.POEntry(
            msgid=msgid,
            msgstr=msgstr,
            comment=field,
        )
        return entry

    def create_po_object(self):
        """Create PO file with some metadata."""
        po_object = polib.POFile()
        po_object.metadata = {
            "Project-Id-Version": "1.0",
            "Report-Msgid-Bugs-To": f"{self.bug_mail}",
            "MIME-Version": "1.0",
            "Content-Type": "text/plain; charset=utf-8",
            "Content-Transfer-Encoding": "8bit",
            "PO-Revision-Date": f"{self.current_time}",
        }
        return po_object

    def cleanup_on_error(self):
        """Remove created files on error."""
        print("Error. Cleaning up the files that were already created.")
        for dir in os.listdir(self.export_dir):
            with contextlib.suppress(FileNotFoundError):
                os.remove(os.path.join(self.export_dir, dir, "LC_MESSAGES/content.po"))
                os.remove(os.path.join(self.export_dir, dir, "LC_MESSAGES/content.mo"))
            os.rmdir(os.path.join(self.export_dir, dir, "LC_MESSAGES"))
            os.rmdir(os.path.join(self.export_dir, dir))
        os.rmdir(self.export_dir)


class OptionalFieldTranslationMissing(Exception):  # noqa: D101
    def __init__(self, translatable_object, translatable_field, *args):
        super().__init__(args)
        self.translatable_field = translatable_field
        self.translatable_object = translatable_object

    def __str__(self):
        return (
            f"The object {self.translatable_object} is missing a translation for the optional field "
            f"{self.translatable_field}, that is present in the default language."
        )


class ObjectNotTranslatedInSourceLanguage(Exception):  # noqa: D101
    def __init__(self, translatable_object, source_lang, *args):
        super().__init__(args)
        self.source_lang = source_lang
        self.translatable_object = translatable_object

    def __str__(self):
        return (
            f"The source language {self.source_lang} is missing a translation for the object "
            f"{getattr(self.translatable_object, 'id', 'NO_ID')}: `{self.translatable_object}`"
        )
