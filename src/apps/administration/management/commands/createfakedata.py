"""Populates our database with faked data."""

import random
from argparse import BooleanOptionalAction
from django.core.management.base import BaseCommand
from apps.organizations.factories import OrganizationFactory, ServiceFactory
from apps.filtercategories.factories import (
    FeatureCategoryFactory,
    LanguageCategoryFactory,
    ClassificationCategoryFactory,
    ServiceCategoryFactory,
    TopicCategoryFactory,
)
from apps.site_settings.models import SettingsTranslation
from apps.flatpages_parler.factories import ParlerFlatPageFactory
from apps.locations.factories import PhysicalLocationFactory
from apps.site_settings.factories import SettingsTranslationFactory


class Command(BaseCommand):  # noqa: D101
    help = "Creates a fake radarini dataset"

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument("--num-organizations", type=int, default=50)
        parser.add_argument("--num-features", type=int, default=30)
        parser.add_argument("--num-languages", type=int, default=10)
        parser.add_argument("--num-classifications", type=int, default=5)
        parser.add_argument("--num-servicecategories", type=int, default=50)
        parser.add_argument("--num-topics", type=int, default=30)
        parser.add_argument(
            "--skip-filtercategories", type=bool, action=BooleanOptionalAction
        )
        parser.add_argument(
            "--skip-organisations", type=bool, action=BooleanOptionalAction
        )
        parser.add_argument("--skip-flatpages", type=bool, action=BooleanOptionalAction)
        parser.add_argument(
            "--skip-site-settings", type=bool, action=BooleanOptionalAction
        )
        return

    def handle(self, *args, **options):  # noqa: D102
        if not options["skip_filtercategories"]:
            self._create_filtercategories(**options)
        if not options["skip_organisations"]:
            self._create_organizations_with_services(**options)
        if not options["skip_flatpages"]:
            self._create_flatpages()
        if not options["skip_site_settings"]:
            self._create_site_settings()

    def _create_filtercategories(
        self,
        num_features: int,
        num_languages: int,
        num_classifications: int,
        num_servicecategories: int,
        num_topics: int,
        **kwargs,
    ):
        for _ in range(num_features):
            FeatureCategoryFactory()
        for _ in range(num_languages):
            LanguageCategoryFactory()
        for _ in range(num_classifications):
            ClassificationCategoryFactory()
        for _ in range(num_servicecategories):
            ServiceCategoryFactory()
        for _ in range(num_topics):
            TopicCategoryFactory()

    def _create_site_settings(self):
        for settings_translation in SettingsTranslation.objects.all():
            settings_translation.delete()
        SettingsTranslationFactory()

    def _create_organizations_with_services(
        self,
        num_organizations: int,
        min_services: int = 1,
        max_services: int = 15,
        **kwargs,
    ):
        for _ in range(num_organizations):
            orga = OrganizationFactory(state=self._random_organization_state())
            location = PhysicalLocationFactory()
            num_services = random.randint(min_services, max_services)
            for _ in range(num_services):
                ServiceFactory(organization=orga, physicallocations=[location])

    def _random_organization_state(self):
        state_weights = {
            "approved": 0.7,
            "rejected": 0.05,
            "in_review": 0.1,
            "pending": 0.05,
            "new": 0.1,
            "deleted": 0.0,
        }
        return random.choices(
            list(state_weights.keys()), weights=list(state_weights.values())
        )[0]

    def _create_flatpages(self):
        urls = [
            "/about/",
            "/contact/",
            "/details-classifications/",
            "/details-features/",
            "/details-services/",
            "/details-topics/",
            "/donate/",
            "/financing/",
            "/form-intro/",
            "/idea/",
            "/imprint/",
            "/participate/",
            "privacy-policy/",
            "/welcome/",
        ]
        for url in urls:
            ParlerFlatPageFactory(url=url)
