# noqa: D100
import factory
from django.conf import settings


class FeatureCategoryFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake feature category entities."""

    class Meta:  # noqa: D106
        model = "filtercategories.FeatureCategory"

    name = factory.Faker("job")
    description = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)


class ClassificationCategoryFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake classification category entities."""

    class Meta:  # noqa: D106
        model = "filtercategories.ClassificationCategory"

    name = factory.Faker("word")
    description = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)


class LanguageCategoryFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake language category entities."""

    class Meta:  # noqa: D106
        model = "filtercategories.LanguageCategory"

    name = factory.Faker("country")
    iso_code = factory.Faker("country_code")


class TopicCategoryFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake topic category entities."""

    class Meta:  # noqa: D106
        model = "filtercategories.TopicCategory"

    name = factory.Faker("color_name")
    description = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)


class ServiceCategoryFactory(factory.django.DjangoModelFactory):
    """Factory to generate fake service category entities."""

    class Meta:  # noqa: D106
        model = "filtercategories.ServiceCategory"

    name = factory.Faker("job")
    description = factory.Faker("sentence", locale=settings.FACTORY_LOCALE)
