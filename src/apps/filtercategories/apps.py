from django.apps import AppConfig


class FiltercategoriesConfig(AppConfig):
    """Config for the featurecatgories app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.filtercategories"
