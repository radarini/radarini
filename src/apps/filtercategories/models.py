from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from parler.fields import TranslatedField

from parler.models import TranslatableModel, TranslatedFields
from mptt.models import MPTTModel, TreeForeignKey

from .managers import ParlerMPTTManager


class CategoryBaseModel(models.Model):
    """Base model for filter categories."""

    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:  # noqa: D106
        abstract = True


class FeatureCategory(TranslatableModel, CategoryBaseModel):
    """Filter category for a specific property of an entry."""

    name = TranslatedField(any_language=True)
    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        description=models.TextField(_("description"), blank=True),
    )

    def __str__(self):
        return self.name

    class Meta:  # noqa: D106
        verbose_name = "feature"
        verbose_name_plural = "features"


class ClassificationCategory(TranslatableModel, CategoryBaseModel):
    """Filter category for the classification of an entry."""

    name = TranslatedField(any_language=True)
    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        description=models.TextField(_("description"), blank=True),
        any_language=True,
    )

    def __str__(self):
        return self.name

    class Meta:  # noqa: D106
        verbose_name = "classification"
        verbose_name_plural = "classifications"


class LanguageCategory(TranslatableModel, CategoryBaseModel):
    """Filter category for the language of an entry."""

    iso_code = models.CharField(max_length=2)
    translations = TranslatedFields(
        name=models.CharField(_("description"), max_length=250), any_language=True
    )

    def __str__(self):
        return f"{self.name} [{self.iso_code}]"

    class Meta:  # noqa: D106
        verbose_name = "language"
        verbose_name_plural = "languages"


class TopicCategory(MPTTModel, TranslatableModel, CategoryBaseModel):
    """Filter category for the topic of an entry."""

    parent = TreeForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="children"
    )

    name = TranslatedField(any_language=True)
    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        description=models.TextField(_("description"), blank=True),
    )

    objects = ParlerMPTTManager()

    def __str__(self):
        return self.safe_translation_getter("name", any_language=True)

    class Meta:  # noqa: D106
        verbose_name = "topic"
        verbose_name_plural = "topics"


class ServiceCategory(MPTTModel, TranslatableModel, CategoryBaseModel):
    """Filter category for the service of an entry."""

    parent = TreeForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="children"
    )

    name = TranslatedField(any_language=True)
    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        description=models.TextField(_("description"), blank=True),
    )

    objects = ParlerMPTTManager()

    def __str__(self):
        return self.safe_translation_getter("name", any_language=True)

    class Meta:  # noqa: D106
        verbose_name = "service category"
        verbose_name_plural = "service categories"
