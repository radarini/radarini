from parler.managers import TranslatableManager, TranslatableQuerySet
from mptt.managers import TreeManager
from mptt.querysets import TreeQuerySet


class ParlerMPTTQuerySet(TranslatableQuerySet, TreeQuerySet):
    """Translatable querysets with Parler and MPTT."""

    def as_manager(cls):  # noqa: D102
        # make sure creating managers from querysets works.
        manager = ParlerMPTTManager.from_queryset(cls)()
        manager._built_with_as_manager = True
        return manager

    as_manager.queryset_only = True
    as_manager = classmethod(as_manager)


class ParlerMPTTManager(TreeManager, TranslatableManager):
    """Combines MPTT and Parler managers."""

    _queryset_class = ParlerMPTTQuerySet
