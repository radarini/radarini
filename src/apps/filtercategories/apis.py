"""This module contains the API for the filter categories."""

from apps.filtercategories.models import (
    ClassificationCategory,
    LanguageCategory,
    TopicCategory,
    FeatureCategory,
    ServiceCategory,
)
from apps.locations.models import AreaOfRelevance
from django.http import JsonResponse
from django.utils.translation import gettext_lazy as _
from django.views.decorators.vary import vary_on_cookie


@vary_on_cookie
def index(request):
    """Return all filter category names as json."""
    filters = {
        "areas_of_relevance": {},
        "organization__topics": {},
        "service_category": {},
        "classifications": {},
        "languages": {},
        "features": {},
        "recurring_days": {},
    }

    for area in AreaOfRelevance.objects.all():
        filters["areas_of_relevance"][area.id] = area.name

    for topic in TopicCategory.objects.all():
        filters["organization__topics"][topic.id] = topic.name

    for service in ServiceCategory.objects.all():
        filters["service_category"][service.id] = service.name

    for classification in ClassificationCategory.objects.all():
        filters["classifications"][classification.id] = classification.name

    for language in LanguageCategory.objects.all():
        filters["languages"][language.id] = language.name

    for feature in FeatureCategory.objects.all():
        filters["features"][feature.id] = feature.name

    filters["recurring_days"] = {
        0: _("Mo"),
        1: _("Tu"),
        2: _("We"),
        3: _("Th"),
        4: _("Fr"),
        5: _("Sa"),
        6: _("Su"),
    }

    return JsonResponse(filters)
