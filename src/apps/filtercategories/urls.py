from django.urls import path
from . import views
from . import apis


app_name = "filtercategories"
urlpatterns = [
    path("topics/", views.Topics.as_view(), name="topics"),
    path("services/", views.Services.as_view(), name="services"),
    path("features/", views.Features.as_view(), name="features"),
    path("classifications/", views.Classifications.as_view(), name="classifications"),
    path("api/", apis.index, name="api"),
]
