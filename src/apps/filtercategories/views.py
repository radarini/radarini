from django.shortcuts import render
from django.views import View
from django.core.exceptions import ObjectDoesNotExist
from apps.flatpages_parler.models import ParlerFlatPage
from apps.filtercategories.models import (
    TopicCategory,
    ServiceCategory,
    FeatureCategory,
    ClassificationCategory,
)


class FilterList(View):
    """Base view for filter endpoint."""

    model = None
    flatpage = None
    filtercatecorgy_name = None

    def get(self, request):  # noqa: D102
        list = self.model.objects.all()
        try:
            flatpage = ParlerFlatPage.objects.get(url=self.flatpage)
        except ObjectDoesNotExist:
            flatpage = None
        context = {
            "list": list,
            "filtercatecorgy_name": self.filtercatecorgy_name,
            "flatpage": flatpage,
        }
        return render(request, "list.html", context)


class Topics(FilterList):
    """View for topic filter."""

    model = TopicCategory
    flatpage = "/details-topics/"
    filtercatecorgy_name = "topics"


class Services(FilterList):
    """View for service filter."""

    model = ServiceCategory
    flatpage = "/details-services/"
    filtercatecorgy_name = "services"


class Classifications(FilterList):
    """View for classification filter."""

    model = ClassificationCategory
    flatpage = "/details-classifications/"
    filtercatecorgy_name = "classifications"


class Features(FilterList):
    """View for feature filter."""

    model = FeatureCategory
    flatpage = "/details-features/"
    filtercatecorgy_name = "features"
