from django.contrib import admin

from parler.admin import TranslatableAdmin
from parler.forms import TranslatableModelForm
from mptt.admin import MPTTModelAdmin
from mptt.forms import MPTTAdminForm

from .models import (
    FeatureCategory,
    ClassificationCategory,
    TopicCategory,
    ServiceCategory,
    LanguageCategory,
)


class ParlerMPTTAdminForm(MPTTAdminForm, TranslatableModelForm):
    """Form for translatable model admin."""

    pass


class ParlerMPTTAdmin(TranslatableAdmin, MPTTModelAdmin):
    """Translatable model admin."""

    form = ParlerMPTTAdminForm


admin.site.register(FeatureCategory, TranslatableAdmin)
admin.site.register(ClassificationCategory, TranslatableAdmin)
admin.site.register(TopicCategory, ParlerMPTTAdmin)
admin.site.register(ServiceCategory, ParlerMPTTAdmin)
admin.site.register(LanguageCategory, TranslatableAdmin)
