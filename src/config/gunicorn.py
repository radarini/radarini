"""Gunicorn production config file."""

import multiprocessing
import os

# Django WSGI application path in pattern MODULE_NAME:VARIABLE_NAME
wsgi_app = "config.wsgi:application"
# The number of worker processes for handling requests
workers = multiprocessing.cpu_count() * 2 + 1

# See https://pythonspeed.com/articles/gunicorn-in-docker/
worker_tmp_dir = "/dev/shm"
# The socket to bind
port = os.environ.get("PORT")
bind = f"0.0.0.0:{port}"
timeout = 1000000
# Write access and error info to /var/log
accesslog = "/app/log/gunicorn/access.log"
errorlog = "/app/log/gunicorn/error.log"
# Redirect stdout/stderr to log file

capture_output = True
