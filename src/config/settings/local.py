"""Local settings for development environment."""

from config.settings.base import *  # noqa: F403

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: don't keep this default secret key used in production secret!
SECRET_KEY = "django-insecure-oplzu@ii#x4=(x(30q4@=(hx+nrnp04sr(l($0%2g&4hsf#ez+"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

# Email configuration
EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
EMAIL_FILE_PATH = BASE_DIR / "sent_emails"  # noqa: F405
ERROR_REPORT_MAIL = "admin@example.org"
DEFAULT_FROM_EMAIL = "admin@example.org"
SUBMISSION_REPORT_MAIL = "admin@example.org"
SUPPORT_MAIL = "support@example.org"

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.spatialite",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),  # noqa: F405
    }
}
