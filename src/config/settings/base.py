"""
Django settings for radarini.
"""

from pathlib import Path
from iso639 import Lang
import os

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent

HOST = os.environ.get("HOST")

# Application definition
INSTALLED_APPS = [
    # Our apps
    "apps.organizations",
    "apps.filtercategories",
    "apps.locations",
    "apps.flatpages_parler",
    "apps.theme",
    "apps.site_settings",
    "apps.administration",
    "apps.accounts.apps.AccountsConfig",
    # Third party apps
    "polymorphic",
    "parler",
    "tailwind",
    "django_htmx",
    "markdownify.apps.MarkdownifyConfig",
    "django_filters",
    "mptt",
    "django_fsm",
    "formtools",
    "django_browser_reload",
    "django_fsm_log",
    "compressor",
    "qr_code",
    "honeypot",
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.flatpages",
    "django.contrib.sitemaps",
    "django.contrib.gis",
]

SITE_ID = 1

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "csp.middleware.CSPMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django_htmx.middleware.HtmxMiddleware",
    "django.contrib.flatpages.middleware.FlatpageFallbackMiddleware",
    "django_browser_reload.middleware.BrowserReloadMiddleware",
]

ROOT_URLCONF = "config.urls"

CSP_MEDIA_SRC = ["'self'", "data:"]
CSP_SCRIPT_SRC = [
    "'self'",
    "'unsafe-eval'",
    "'unsafe-inline'",
]
CSP_STYLE_SRC = ["'self'", "'unsafe-inline'"]
CSP_IMG_SRC = [
    "'self'",
    "data:",
    "https://tile.openstreetmap.org",
]
CSP_DEFAULT_SRC = ["'self'"]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "apps.site_settings.context_processors.site_settings_processor",
            ],
        },
    },
]

WSGI_APPLICATION = "config.wsgi.application"

# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

PARLER_ENABLE_CACHING = False  # Parler caching is buggy, keep it disabled

LANGUAGE_CODE = os.environ.get("DEFAULT_LANGUAGE", "en")
PARLER_LANGUAGES = {
    1: (),
    "default": {
        "fallbacks": [LANGUAGE_CODE],  # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        "hide_untranslated": False,  # let .active_translations() return fallbacks too
    },
}
LANGUAGES = []
LANGUAGES_ENV = [item.strip() for item in os.environ.get("LANGUAGES", "en").split(",")]
for language in LANGUAGES_ENV:
    PARLER_LANGUAGES[1] += ({"code": language},)
    LANGUAGES.append((language, Lang(language).name))

LOCALE_PATHS = [
    os.path.join(BASE_DIR, "locale"),
]

FACTORY_LOCALE = "de_DE"  # locale for generated test data
FACTORY_COORDINATES_LAT = (
    52.520008  # latitude of center coordinates for generated test data
)
FACTORY_COORDINATES_LNG = (
    13.404954  # longitude of center coordinates for generated test data
)
FACTORY_SRID = 4326  # Spatial reference system for generated test data

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = "static/"

STATIC_ROOT = BASE_DIR / "staticfiles"

STATICFILES_DIRS = [BASE_DIR / "static"]

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]

# Offline compression is required for compatibility with whitenoise
# See https://whitenoise.readthedocs.io/en/latest/django.html#django-compressor
COMPRESS_OFFLINE = True
COMPRESS_STORAGE = "compressor.storage.BrotliCompressorFileStorage"

STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        # Append content hashes to static files so we cause cache busts on updates
        "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
    },
}


# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# django-tailwind (CSS framework)
TAILWIND_APP_NAME = "apps.theme"

INTERNAL_IPS = [
    "127.0.0.1",
]

# Media / uploads
MEDIA_URL = "media/"

MEDIA_ROOT = os.path.join(BASE_DIR, "data", "media")

# pagination for organizations; items per page
PAGINATION_PER_PAGE = 10

# limit organization logo size in bytes
MAX_LOGO_UPLOAD_SIZE = 1024 * 1024 * 3  # 3 MB

# User Login/Logout URLs
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

MARKDOWNIFY = {
    "default": {
        "WHITELIST_TAGS": [
            "pre",
            "a",
            "abbr",
            "acronym",
            "b",
            "blockquote",
            "em",
            "i",
            "li",
            "ol",
            "p",
            "strong",
            "ul",
            "img",
            "details",
            "summary",
        ],
        "WHITELIST_ATTRS": ["src", "href"],
    }
}

DATA_UPLOAD_MAX_NUMBER_FIELDS = 2000

HONEYPOT_FIELD_NAME = "phone_number"
