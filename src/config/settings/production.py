"""Production settings."""

from config.settings.base import *  # noqa: F403, F401
from glob import glob
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ["SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: update this when you have the production host
ALLOWED_HOSTS = os.environ["ALLOWED_HOSTS"].split(" ")

CSRF_TRUSTED_ORIGINS = []
for host in ALLOWED_HOSTS:
    CSRF_TRUSTED_ORIGINS.append(f"https://{host}")

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = os.environ.get("EMAIL_HOST")
EMAIL_PORT = os.environ.get("EMAIL_PORT")
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")
EMAIL_SUBJECT_PREFIX = f"[{os.environ.get('HOST')}] "

if os.environ.get("EMAIL_PROTOCOL") == "SSL":
    EMAIL_USE_SSL = True
if os.environ.get("EMAIL_PROTOCOL") == "TLS":
    EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
ERROR_REPORT_MAIL = os.environ.get("ERROR_REPORT_MAIL")
SUBMISSION_REPORT_MAIL = os.environ.get("SUBMISSION_REPORT_MAIL")
SUPPORT_MAIL = os.environ.get("SUPPORT_MAIL")

ADMINS = [("Admin", ERROR_REPORT_MAIL)]

# Configure HSTS
SECURE_HSTS_SECONDS = 2_592_000  # 30 days
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True

SECURE_REFERRER_POLICY = "strict-origin-when-cross-origin"

# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-content-type-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "DENY"
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-ssl-redirect
# SECURE_SSL_REDIRECT = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-proxy-ssl-header
# SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_NAME"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": os.environ.get("DB_HOST"),
        "PORT": 5432,
    }
}

# Necessary for Django to find the GDAL and GEOS libraries in an Alpine-based Docker container
GDAL_LIBRARY_PATH = glob("/usr/lib/libgdal.so.*")[0]
GEOS_LIBRARY_PATH = glob("/usr/lib/libgeos_c.so.*")[0]
