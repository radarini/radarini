# Radarini

[![License](https://img.shields.io/badge/license-AGPL--v3-blue)](https://codeberg.org/radarini/radarini/src/branch/main/LICENSE.md)
[![Woodpecker](https://ci.codeberg.org/api/badges/9571/status.svg)](https://ci.codeberg.org/repos/9571)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![uv](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/uv/main/assets/badge/v0.json)](https://github.com/astral-sh/uv)
[![Chat](https://img.shields.io/matrix/radarini-general:matrix.org)](https://matrix.to/#/#radarini-space:systemli.org)
[![Weblate](https://hosted.weblate.org/widgets/radarini/-/svg-badge.svg)](https://hosted.weblate.org/engage/radarini/)

Radarini is a web platform to submit and search for solidarity initiatives, their offers and contact data.

## Installation / Setup

### Development

To run the development setup you need to have the python dependencies available in your python enviroment. We use [**uv**](https://docs.astral.sh/uv/) as package manager. To set up the enviroment simply type `uv sync` in the root directory. Now you should be in an virtual enviroment (sourced by default from `.venv`) with all dependencies installed.

### Production

The production deployment is based on [**docker**](https://docs.docker.com/get-docker/). Using the [docker-compose file](./docker/docker-compose.yml), the main application will be started together with its [supplementary containers](https://codeberg.org/radarini/radarini/wiki/Container-setup). To build the project, the [**buildx plugin**](https://docs.docker.com/build/architecture/#buildx) is required, since we use a [multi-stage](https://docs.docker.com/build/building/multi-stage/) Dockerfile.

## Usage

1. Set the environment variables as described in [example.env](docker/example.env). You can set them directly in your environment, or create a `.env` file in the docker folder.
2. Start the application with `./radarini start prod` or `./radarini start dev` respectively.

**Notes:**
- To work on the application it is useful to fill it with some dummy data. We created a command for this: `./radarini manage <dev/prod> createfakedata`.
- To create a superuser for logging in the admin interface at `/admin` just run `./radarini manage <dev/prod> createsuperuser`.
- You can run further Django admin commands with `./radarini manage prod <command>` (A list of all commands available is displayed via the `help` command).
- Further instructions can be found in the [Admin Manual](https://codeberg.org/radarini/radarini/wiki/Admin-manual).

## Contributing

If you want to contribute to the project, also take a look at the [CONTRIBUTING.md](CONTRIBUTING.md) file.

## Translations

The project is translated via [weblate](https://hosted.weblate.org/projects/radarini/webapp/). At the moment the following languages are translated:

<a href="https://hosted.weblate.org/engage/radarini/">
<img src="https://hosted.weblate.org/widget/radarini/webapp/multi-blue.svg" alt="Translation status" />
</a>

## Donations

If you want to support the project financially, you can find information on [iniradar.org/donate](https://iniradar.org/donate/). Radarini-specific donation options might be added later.

## Roadmap

Our milestones for the project can be found on [codeberg](https://codeberg.org/radarini/radarini/milestones).

## Contributors

- [kiwi](https://codeberg.org/kiwi)
- [lino](https://codeberg.org/lino)
- [overflw](https://codeberg.org/overflw)
- [UNI:CODE IT Solutions GmbH](https://unicode-it.de/) (initial development)
