# Contributing

This is an open project. We are happy if you want to contribute something. In order to get you involved in the workflow and to create a productive working environment in which everyone feels comfortable, it is important that you read this document and the [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md) carefully.

## Communication

The main channel to discuss this project is the matrix space [radarini-space:systemli.org](https://matrix.to/#/#radarini-space:systemli.org). You can join there and just write whatever you want to say or ask.

## Teams

There are different teams with different privileges. Have a look at the [teams overview](https://codeberg.org/org/radarini/teams) to understand them. This functionality should not create an exclusive club, but protect against the project becoming messed up, intentionally or not. If you want to become member of a team just ask for that via the [communication channel](#communication).

## Coordination

If you want to change something in the project, you have to create an issue for that. This helps us to keep track of all the ideas and discuss them in a productive way. There are templates for the different issue types we use. You can select one of them, when creating an [issue on codeberg](https://codeberg.org/radarini/radarini/issues) and then follow the instructions provided there.

The bigger picture of the development is organized in the [milestones](https://codeberg.org/radarini/radarini/milestones).

## Git workflow

The _main_ branch represents the current version of the project. Only critical fixes and minor changes could be commited by the core-devs directly. If not obvious, a description in the commit should explain why this change was necessary. Every other change should be created in a feature branch and introduced with a pull-request. For detailed instructions see the [git workflow manual](https://codeberg.org/radarini/radarini/wiki/Git-workflow.-).

## Versioning and releases

This project uses continuous delivery. So every accepted change is directly delivered via the _main_ branch. The latest commit of this branch always represents the latest version of the project. To reference a specific version use the commit hash.

## Documentation

The documentation for this project can be found in the [codeberg wiki](https://codeberg.org/radarini/radarini/wiki). Feel free to add or change the information there too. Some wiki pages that might help you get started are:

- [Tech-stack](https://codeberg.org/radarini/radarini/wiki/Tech-Stack)
- [Staging-server](https://codeberg.org/radarini/radarini/wiki/Staging-server)
- [Development-setup](https://codeberg.org/radarini/radarini/wiki/Development-setup)

## Acknowledgment

If you want to be listed as a contributor, just add your name in the _README.md_ file.
